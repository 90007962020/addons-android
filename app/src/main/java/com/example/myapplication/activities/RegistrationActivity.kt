package com.example.myapplication

import android.app.ActivityOptions
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.service.Constants
import com.example.myapplication.service.User
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RegistrationActivity : AppCompatActivity(), View.OnClickListener, TextWatcher {
    var firstNameText: String? = null
    var lastNameText: String? = null
    var passwordText: String? = null
    var confirmPasswordText: String? = null
    var emailText: String? = null
    var mobileNumberText: String? = null

    var firstName: EditText? = null
    var lastName: EditText? = null
    var password: EditText? = null
    var confirmPassword: EditText? = null
    var email: EditText? = null
    var mobileNumber: EditText? = null

    private lateinit var database: DatabaseReference
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "PREF_NAME"
    private val USER_NAME = "USER_NAME"
    private val PASSWORD = "PASSWORD"
    lateinit var sharedPref: SharedPreferences

    override fun afterTextChanged(editable: Editable?) {
        if (editable == firstName!!.editableText) firstNameText = editable.toString()
        if (editable == lastName!!.editableText) lastNameText = editable.toString()
        if (editable == email!!.editableText) emailText = editable.toString()
        if (editable == mobileNumber!!.editableText) mobileNumberText = editable.toString()
        if (editable == password!!.editableText) passwordText = editable.toString()
        if (editable == confirmPassword!!.editableText) confirmPasswordText = editable.toString()
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.register -> {

                if (validateUser()) {
                    val hashMap: HashMap<String, Any> = HashMap<String, Any>()

                    hashMap.put("email", email!!.text.toString())
                    hashMap.put("firstName", firstName!!.text.toString())
                    hashMap.put("lastName", lastName!!.text.toString())
                    hashMap.put("login", true)
                    hashMap.put("mobileNumber", mobileNumber!!.text.toString())
                    hashMap.put("password", password!!.text.toString())
                    postUser = hashMap
                    postReferenceAll.addValueEventListener(postListenerAll)
                    //(postUser as HashMap<String, Any>)?.put(email!!.text.toString().replace(".", ","), hashMap)
                    writeNewUser(
                        encodeString(email!!.text.toString()),
                        firstName!!.text.toString(),
                        lastName!!.text.toString(),
                        password!!.text.toString(),
                        mobileNumber!!.text.toString(),
                        email!!.text.toString()
                    )
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        startActivity(
                            Intent(this, HomeActivity::class.java),
                            ActivityOptions.makeSceneTransitionAnimation(this).toBundle()
                        )
                    } else {
                        startActivity(Intent(this, HomeActivity::class.java))
                    }

                    val editor = sharedPref.edit()
                    editor.putString(USER_NAME, email!!.text.toString())
                    editor.putString(PASSWORD, password!!.text.toString())
                    editor.putString(Constants.FIRST_NAME, firstName!!.text.toString())
                    editor.putString(Constants.LAST_NAME, lastName!!.text.toString())
                    editor.putString(Constants.MOBILE_NUMBER, mobileNumber!!.text.toString())
                    editor.apply()
                }
            }
        }
    }

    private fun validateUser(): Boolean {
        var result = true
        if (TextUtils.isEmpty(firstName!!.text.toString())) {
            firstName!!.error = "First Required"
            result = false
        } else {
            firstName!!.error = null
        }

        if (TextUtils.isEmpty(lastName!!.text.toString())) {
            lastName!!.error = "Last Name Required"
            result = false
        } else {
            lastName!!.error = null
        }

        if (TextUtils.isEmpty(password!!.text.toString())) {
            password!!.error = "Password Required"
            result = false
        } else {
            password!!.error = null
        }

        if (TextUtils.isEmpty(confirmPassword!!.text.toString())) {
            confirmPassword!!.error = "Confirm Password"
            result = false
        } else {
            confirmPassword!!.error = null
        }


        if (TextUtils.isEmpty(email!!.text.toString())) {
            email!!.error = "Email ID Required"
            result = false
        } else {
            email!!.error = null
        }

        /*if(TextUtils.isEmpty(password!!.text.toString()) && !TextUtils.isEmpty(confirmPassword!!.text.toString()) &&!(password!!.text.equals(confirmPassword!!.text))){
            password!!.error = "Password Mismatch"
            confirmPassword!!.error = "Password Mismatch"
            result = false
        }else{
            password!!.error = null
            confirmPassword!!.error = null
        }*/

        if (TextUtils.isEmpty(mobileNumber!!.text.toString())) {
            mobileNumber!!.error = "Mobile Number Required"
            result = false
        } else {
            mobileNumber!!.error = null
        }

        return result
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.fragment_registration)
        sharedPref = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        database = FirebaseDatabase.getInstance().reference
        firstName = findViewById(R.id.firstName)
        lastName = findViewById(R.id.lastName)
        password = findViewById(R.id.password)
        confirmPassword = findViewById(R.id.confirmPassword)
        email = findViewById(R.id.email)
        mobileNumber = findViewById(R.id.mobileNumber)
        var register = findViewById<TextView>(R.id.register)
        register.setOnClickListener(this)
        firstName!!.addTextChangedListener(this)
        lastName!!.addTextChangedListener(this)
        password!!.addTextChangedListener(this)
        confirmPassword!!.addTextChangedListener(this)
        email!!.addTextChangedListener(this)
        mobileNumber!!.addTextChangedListener(this)


    }

    private fun writeNewUser(
        userId: String, firstName: String, lastName: String, password: String, mobile: String,
        email: String?
    ) {
        val user = User(firstName, lastName, password, mobile, email)
        var mref: DatabaseReference = database.ref.child("users")
        mref.child(userId!!).setValue(user)

    }

    fun encodeString(string: String): String {
        return string.replace(".", ",")
    }
}
