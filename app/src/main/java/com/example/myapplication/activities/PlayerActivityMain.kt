package com.example.myapplication.activities

import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.pm.ActivityInfo
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.Window
import android.view.WindowManager
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.firstName
import com.example.myapplication.lastName
import com.example.myapplication.service.PersonsModel
import com.example.myapplication.service.PersonsModelShare
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.PlaybackParameters
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.DefaultTimeBar
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.ui.TimeBar
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.RawResourceDataSource
import com.google.android.exoplayer2.util.Util
import com.google.firebase.database.DatabaseReference
import com.sinch.android.rtc.SinchClient
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import pl.droidsonroids.gif.GifImageView
import java.util.concurrent.TimeUnit
import kotlin.concurrent.fixedRateTimer
import kotlin.concurrent.thread

var personsButton: ImageView? = null
var callButton: ImageView? = null
var hangupButton: ImageView? = null
var muteButton: ImageView? = null
var unmuteButton: ImageView? = null

var callButtonIncoming: GifImageView? = null
var share: ImageView? = null
var progressBar: DefaultTimeBar? = null
var videoView: PlayerView? = null
private var layoutManager: RecyclerView.LayoutManager? = null
var player: ExoPlayer? = null
lateinit var unmutePersons: ArrayList<String>
lateinit var personsListGroup: ArrayList<PersonsModelShare>
var fastForward: ImageButton? = null
var rewind: ImageButton? = null
var txtPosition: TextView? = null
var txtDuration: TextView? = null
var imgPlay: ImageButton? = null
var imgPause: ImageButton? = null
private var forwardState: Boolean = false
private var rewindState: Boolean = false
private var PRIVATE_MODE = 0
private val PREF_NAME = "PREF_NAME"
private val USER_NAME = "USER_NAME"
lateinit var sharedPref: SharedPreferences
private var userName: String = ""
var dummyUser: String = " "
var sinchClient: SinchClient? = null
private var count: Int? = 0
var size: Int = 0
var random: Int = 0
var dataShare: HashMap<*, *>? = null
var dataNewEvent: HashMap<*, *>? = null
var Owner: String = ""
var creatorId: String = ""
var firstName_lastName: String? = null
var mreciever: BroadcastReceiver? = null
var bluetoothReciever: BroadcastReceiver? = null
var filterIntent: IntentFilter? = null
var filterBluetooth: IntentFilter? = null
var audioManager: AudioManager? = null
lateinit var postReferenceShareEventAdTime: DatabaseReference
lateinit var newEventpostReferenceShareEventAdTime: DatabaseReference
var position: Int = -1
var isShareEvent: Boolean = false

var isNewEvent: Boolean = false
var outgoingUserId: String = ""
var incomingUserId: String = ""
var isCurrentUserGroupCall: Boolean = false
var incomingUsersList: ArrayList<String>? = null
var calledUserList: ArrayList<String>? = null
lateinit var shareEventParticipantList: ArrayList<PersonsModel>
var startTimer: Boolean = false

var isAdsEnabled: Boolean = false
var playbackDisposable: Disposable? = null
var isSinchGroupCall = false
var lockStatus: Boolean = false

class PlayerActivityMain : AppCompatActivity(), View.OnClickListener, Player.EventListener,
    TimeBar.OnScrubListener, View.OnLongClickListener {
    var adTime: Long = 40
    var adNumber: Long = 1
    var isHangupButton: Boolean = false
    var isCallButton: Boolean = false
    var isMuteButton: Boolean = false
    var isUnmuteButton: Boolean = false
    var isForward: Boolean = false
    var isRewind: Boolean = false

    private var playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition: Long = 0
    private var playButton: ImageButton? = null
    private var pauseButton: ImageButton? = null

    override fun onLongClick(p0: View?): Boolean {
        Log.e("LOng click", "Long click")
        return true
    }

    override fun onScrubMove(timeBar: TimeBar?, position: Long) {
    }

    override fun onScrubStart(timeBar: TimeBar?, position: Long) {
    }

    override fun onScrubStop(timeBar: TimeBar?, position: Long, canceled: Boolean) {
        if (player != null && isShareEvent) {
            if (isAdsEnabled) {
                postReferenceShareEventAdTime.setValue((position / 1000) + 40)
            }
        } else if (player != null && isNewEvent) {
            if (isAdsEnabled /*&& adTime < (position / 1000)*/) {
                newEventpostReferenceShareEventAdTime.setValue((position / 1000) + 40)
            }
        } else {
            if (isAdsEnabled) {
                if (adTime < position / 1000) {
                    adTime = (position / 1000) + 40
                    adNumber = 1
                }
            }
        }
    }

    fun vibrate() {
        val vibrator = this.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            vibrator.vibrate(500)
        }
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        val stateString: String
        when (playbackState) {
            Player.STATE_IDLE -> Log.e("Duration", player?.currentPosition.toString())
            Player.STATE_BUFFERING -> Log.e("Duration", player?.currentPosition.toString())
            Player.STATE_READY -> {
                if (!startTimer) {
                    var playbackProgressObservable: Observable<Long> =
                        Observable.interval(1, TimeUnit.SECONDS).map({ player!!.currentPosition })
                    playbackDisposable = playbackProgressObservable.subscribe { progress ->

                    }
                    startTimer = true
                }
                if (playWhenReady && playbackState == Player.STATE_READY) {
                }

            }
            Player.STATE_ENDED -> {
                if (forwardState != null && rewindState != null) {
                    forwardState = false
                    rewindState = false
                }

            }
        }

    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.exo_pause -> {
                player!!.playWhenReady = false
            }
            R.id.exo_play -> {
                player!!.playWhenReady = true
            }
            R.id.call -> {
                isCurrentUserGroupCall = true
            }
            R.id.hangup -> {
                vibrate()
            }
            R.id.mute -> {
                vibrate()
                muteButton!!.visibility = GONE
                callButton!!.visibility = GONE
                hangupButton!!.visibility = VISIBLE
                unmuteButton!!.visibility = VISIBLE
                isCallButton = false
                isMuteButton = false
                isUnmuteButton = true
                isHangupButton = true
                isCurrentUserGroupCall = true

            }
            R.id.unmute -> {
                vibrate()
                muteButton!!.visibility = VISIBLE
                unmuteButton!!.visibility = GONE
                callButton!!.visibility = GONE
                hangupButton!!.visibility = VISIBLE
                sinchClient!!.audioController.mute()
                isSinchGroupCall = false
                isCallButton = false
                isMuteButton = true
                isUnmuteButton = false
                isHangupButton = true
                isCurrentUserGroupCall = false
            }
            R.id.personsButton_image -> {
                vibrate()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startTimer = false
        personsListGroup = ArrayList<PersonsModelShare>()
        if (playbackDisposable != null) playbackDisposable!!.dispose()
        sharedPref = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        mreciever = HeadSetReceiver()
        bluetoothReciever = bluetoothState()
        filterIntent = IntentFilter(Intent.ACTION_HEADSET_PLUG)
        filterBluetooth = IntentFilter()
        filterBluetooth!!.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        filterBluetooth!!.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        filterBluetooth!!.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        userName = sharedPref.getString(USER_NAME, "")
        incomingUsersList = ArrayList()
        calledUserList = ArrayList()
        dummyUser = userName.replace(".", ",")
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        random = (1..99).shuffled().first()
        shareEventParticipantList = ArrayList()
        unmutePersons = ArrayList()
        count = 0
        size = 0
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_player)
        layoutManager = LinearLayoutManager(this)
        callButton = findViewById(R.id.call) as ImageView
        hangupButton = findViewById(R.id.hangup) as ImageView
        muteButton = findViewById(R.id.mute) as ImageView
        unmuteButton = findViewById(R.id.unmute) as ImageView
        callButtonIncoming = findViewById(R.id.callIncoming) as GifImageView
        personsButton = findViewById(R.id.personsButton_image)
        //check user having events or not
        firstName_lastName = firstName + " " + lastName
        // to mark user as online

        share = findViewById(R.id.share_img)
        share!!.setOnClickListener(this)
        personsButton!!.setOnClickListener(this)
        videoView = findViewById(R.id.videoView1)
        playButton = videoView!!.findViewById(R.id.exo_play)
        pauseButton = videoView!!.findViewById(R.id.exo_pause)
        pauseButton!!.visibility = GONE
        initializePlayer()
        videoView!!.findViewById<ImageButton>(R.id.exo_ffwd).setOnLongClickListener(this)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE

        registerReceiver(bluetoothReciever, filterBluetooth)
        registerReceiver(mreciever, filterIntent)


        fastForward!!.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        onForward(isForward)
                        return true
                    }
                    MotionEvent.ACTION_UP -> {
                        forwardState = false
                        return true
                    }
                }
                return v?.onTouchEvent(event) ?: true
            }

        })

        rewind!!.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        // if (!status) {
                        onRewind(true)
                        return true
                    }
                    MotionEvent.ACTION_UP -> {
                        //if (!status) {

                        onRewind(false)

                        //}
                        return true

                    }
                }
                return v?.onTouchEvent(event) ?: true
            }

        })

    }

    fun onForward(state: Boolean) {
        doAsync {
            if (state) {
                while (isForward) {
                    var param = PlaybackParameters(2f)
                    videoView!!.player.playbackParameters = param!!
                    isForward = false
                }
            } else {
                var param = PlaybackParameters(1f)
                player!!.playbackParameters = param!!
            }
        }
    }

    fun onRewind(state: Boolean) {
        doAsync {
            if (state) {
                fixedRateTimer("timer", false, 0L, 500) {
                    if (isRewind == false) cancel()
                    uiThread {
                        player!!.seekTo(currentWindow, player!!.currentPosition - 1000)
                        playbackPosition = player!!.currentPosition
                        currentWindow = player!!.currentWindowIndex
                        val minutes = playbackPosition / 1000 / 60
                        val seconds = playbackPosition / 1000 % 60
                        if (minutes <= 0 && seconds <= 0) {
                            isRewind = false

                        }
                    }
                    thread {

                    }

                }
            } else {
                var param = PlaybackParameters(1f)
                player!!.playbackParameters = param!!
            }
        }
    }

    private fun initializePlayer() {
        size = 0
        if (player == null) {
            player = ExoPlayerFactory.newSimpleInstance(this)
            videoView?.player = player

        }
        var dataSourceFactory =
            DefaultDataSourceFactory(this, Util.getUserAgent(this, "MyApplication"))
        lateinit var firstSource: MediaSource
        firstSource = ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(RawResourceDataSource.buildRawResourceUri(R.raw.testsong)/*url*/)
        player!!.playWhenReady = playWhenReady
        player!!.seekTo(currentWindow, playbackPosition)
        player!!.prepare(firstSource)
        progressBar = findViewById(R.id.exo_progress)
        fastForward = findViewById(R.id.exo_ffwd)
        rewind = findViewById(R.id.exo_rew)
        txtDuration = findViewById(R.id.exo_duration)
        txtPosition = findViewById(R.id.exo_position)
        imgPlay = findViewById(R.id.exo_play)
        imgPause = findViewById(R.id.exo_pause)
        imgPause!!.setOnClickListener(this)
        imgPlay!!.setOnClickListener(this)
        player!!.addListener(this)
        player!!.playWhenReady = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            progressBar!!.addListener(this)
        }
    }

    override fun onPause() {
        super.onPause()
        if (player != null) {
            player!!.setPlayWhenReady(false)
        }
    }

    override fun onResume() {
        if (player != null) {
            player!!.setPlayWhenReady(true)
        } else {
            initializePlayer()
        }
        super.onResume()

    }

}

class bluetoothState : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.getAction()
        if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
            if (audioManager != null) {
                audioManager!!.startBluetoothSco()
                audioManager!!.setMode(AudioManager.MODE_IN_CALL)
                audioManager!!.setSpeakerphoneOn(false)
            }

        } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
            if (audioManager != null) {
                audioManager!!.stopBluetoothSco()
                audioManager!!.setMode(AudioManager.MODE_IN_CALL)
                audioManager!!.setSpeakerphoneOn(true)
            }
        }

    }
}

class HeadSetReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == (Intent.ACTION_HEADSET_PLUG)) {
            var state = intent.getIntExtra("state", -1)
            when (state) {
                0 -> {
                    if (audioManager != null) {
                        audioManager!!.isSpeakerphoneOn = true
                        audioManager!!.setMode(AudioManager.MODE_IN_CALL)
                    }
                }
                1 -> {
                    if (audioManager != null) {
                        audioManager!!.setSpeakerphoneOn(false)
                        audioManager!!.setMode(AudioManager.MODE_IN_CALL)
                    }
                }

            }
        }
    }


}










