package com.example.myapplication

import android.Manifest
import android.app.ActivityOptions
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.myapplication.activities.dummyUser
import com.example.myapplication.service.Constants
import com.google.firebase.database.*
import com.google.firebase.iid.FirebaseInstanceId
import java.text.SimpleDateFormat
import java.util.*

var isHomeLaunched: Boolean = false
lateinit var postListenerSplash: ValueEventListener
lateinit var postReferenceSplash: DatabaseReference

class SplashActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var userNameText: EditText
    lateinit var passwordText: EditText

    private var PRIVATE_MODE = 0
    private val PREF_NAME = "PREF_NAME"
    private val USER_NAME = "USER_NAME"
    private val PASSWORD = "PASSWORD"
    lateinit var sharedPref: SharedPreferences
    lateinit var progressDialog: ProgressDialog
    var date: String = ""

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.logIn -> {
                onLoginClick()
            }
            /**/
            R.id.signUp -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startActivity(
                    Intent(this, RegistrationActivity::class.java),
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle()
                )
            } else {
                startActivity(Intent(this, RegistrationActivity::class.java))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPref = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        requestWindowFeature(Window.FEATURE_NO_TITLE)


        postListenerSplash = object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.value != null) {
                    progressDialog.dismiss()
                    postUser = dataSnapshot.value as HashMap<*, *>
                    var values = postUser!!.get(
                        userNameText.text.toString().replace(
                            ".",
                            ","
                        )
                    ) as HashMap<*, *>
                    val editor = sharedPref.edit()
                    editor.putString(USER_NAME, userNameText.text.toString())
                    editor.putString(PASSWORD, passwordText.text.toString())
                    editor.putString(Constants.loginTime, date)
                    editor.putString(Constants.FIRST_NAME, values!!.get("firstName").toString())
                    editor.putString(Constants.LAST_NAME, values!!.get("lastName").toString())
                    editor.putString(
                        Constants.MOBILE_NUMBER,
                        values!!.get(Constants.mobileNumber).toString()
                    )
                    editor.commit()
                    editor.apply()

                    postReferenceSplash.child("users").child(dummyUser).child("fcmToken").setValue(
                        FirebaseInstanceId.getInstance().token
                    )


                    postReferenceSplash.removeEventListener(postListenerSplash)
                    startActivity(Intent(this@SplashActivity, HomeActivity::class.java))

                } else {
                    userNameText.error = "User Not Found"
                }
            }
        }


        postReferenceSplash = FirebaseDatabase.getInstance().reference
            .child("users")
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )



        if (postUser != null) {

            if (sharedPref != null && sharedPref.getString(
                    USER_NAME,
                    ""
                ) != "" && sharedPref.getString(
                    PASSWORD, ""
                ) != ""
            ) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(
                        Intent(this@SplashActivity, HomeActivity::class.java),
                        ActivityOptions.makeSceneTransitionAnimation(this@SplashActivity).toBundle()
                    )
                } else {
                    startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                }
            } else {
                setContentView(R.layout.activity_splash)
                val login: TextView = findViewById(R.id.logIn)
                val signUp: TextView = findViewById(R.id.signUp)
                login.setOnClickListener(this@SplashActivity)
                signUp.setOnClickListener(this@SplashActivity)
                userNameText = findViewById(R.id.userName)
                passwordText = findViewById(R.id.password)
                userNameText.addTextChangedListener(object : TextWatcher {

                    override fun afterTextChanged(s: Editable) {
                        var userId = s.toString()
                        if (userId.contains(".com")) {
                            userId = userId.replace(".", ",")
                            var user = post!!["users"] as HashMap<*, *>
                            val keys = user.keys
                            for (key in keys) {
                                if (userId == key) {
                                    postReferenceSplash.child(userId).child("login").setValue(false)
                                }
                            }

                        }
                    }

                    override fun beforeTextChanged(
                        s: CharSequence, start: Int,
                        count: Int, after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence, start: Int,
                        before: Int, count: Int
                    ) {
                    }
                })


                passwordText.setOnEditorActionListener { _, actionId, event ->
                    if ((event != null && (event.keyCode === KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                        onLoginClick()
                    }
                    false
                }


            }
        } else {
            setContentView(R.layout.activity_splash)
            val login: TextView = findViewById(R.id.logIn)
            val signUp: TextView = findViewById(R.id.signUp)
            login.setOnClickListener(this@SplashActivity)
            signUp.setOnClickListener(this@SplashActivity)
            userNameText = findViewById(R.id.userName)
            passwordText = findViewById(R.id.password)

            userNameText.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {
                    Log.e("Text", s.toString())
                }

                override fun beforeTextChanged(
                    s: CharSequence, start: Int,
                    count: Int, after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence, start: Int,
                    before: Int, count: Int
                ) {
                }
            })

            if (ContextCompat.checkSelfPermission(
                    this@SplashActivity,
                    Manifest.permission.READ_CONTACTS
                )
                != PackageManager.PERMISSION_GRANTED
            ) {

                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this@SplashActivity,
                        Manifest.permission.READ_CONTACTS
                    )
                ) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(
                        this@SplashActivity,
                        arrayOf(Manifest.permission.READ_CONTACTS),
                        1
                    )

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
            }
            passwordText.setOnEditorActionListener { _, actionId, event ->
                if ((event != null && (event.keyCode === KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    onLoginClick()
                }
                false
            }

        }


    }

    private fun validateUser(): Boolean {
        var result = true
        if (TextUtils.isEmpty(userNameText.text.toString())) {
            userNameText.error = "UserName Required"
            result = false
        } else {
            userNameText.error = null
        }

        if (TextUtils.isEmpty(passwordText.text.toString())) {
            passwordText.error = "Password Required"
            result = false
        } else {
            passwordText.error = null
        }
        return result
    }

    override fun onBackPressed() {
        finishAffinity()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    fun onLoginClick() {
        if (validateUser() /*&& post!=null*/) {
            progressDialog = ProgressDialog(this)
            progressDialog.setMessage("Loading..")
            progressDialog.setCancelable(false)
            progressDialog.show()
            val f = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            f.timeZone = TimeZone.getTimeZone("UTC")
            date = f.format(Date())

            postReferenceSplash.orderByKey().equalTo(userNameText.text.toString().replace(".", ","))
                .addValueEventListener(
                    postListenerSplash
                )

            postReferenceSplash.child(userNameText.text.toString().replace(".", ","))
                .child(Constants.loginTime).setValue(
                date
            )
        } else {
            val f = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            f.timeZone = TimeZone.getTimeZone("UTC")
            date = f.format(Date())
            val editor = sharedPref.edit()
            editor.putString(USER_NAME, userNameText.text.toString())
            editor.putString(PASSWORD, passwordText.text.toString())
            editor.putString(Constants.loginTime, date)
            editor.commit()
            editor.apply()
            if (post == null) Toast.makeText(this, "Something went wrong!!", LENGTH_LONG).show()
        }
    }

}
