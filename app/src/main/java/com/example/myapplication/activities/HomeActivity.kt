package com.example.myapplication

import android.Manifest
import android.app.ActivityOptions
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.AttributeSet
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.myapplication.activities.PlayerActivityMain
import com.example.myapplication.fragments.*
import com.example.myapplication.service.*
import com.google.android.material.navigation.NavigationView
import com.google.firebase.database.*
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet

var finalShareParticipantsList = ArrayList<PersonsModel>()
var contactsList = ArrayList<ContactsModel>()
var contactsHashMap = HashMap<String, ContactsModel>()
var firstName = ""
var lastName = ""

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    View.OnClickListener, DataListener, UpdateDataListener {
    override fun onRefreshData() {
    }

    lateinit var mListener: UpdateDataListener

    override fun onDataAvailable() {
        requestForContacts()
        Log.e("Listener1", "Listener1")
    }

    var personsFirebaseDummyArrayList = ArrayList<Post>()

    lateinit var progressDialog: ProgressDialog
    private val USER_NAME = "USER_NAME"
    private var PRIVATE_MODE = 0
    private var userName: String = ""
    private val PREF_NAME = "PREF_NAME"
    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val serverKey =
        "key=" + "AAAAJ69Wm6w:APA91bG4g5sA29YmgGoqwYNZ5C2BJ1Qa-mbxsar4AENeb6srNTpEBp-t4xJirdw6cmJ3loytyVLEtWMY1lSGHGr3T8RY1d6e0i34FhZHD3NSqG6narKU7gxNnLCWVxO4XfLWXKyjE4ci"
    private val contentType = "application/json"
    lateinit var selectedList: ArrayList<PersonsModel>
    lateinit var finalselectedList: ArrayList<PersonsModel>
    private lateinit var postReferenceHome: DatabaseReference
    lateinit var postReferenceLoginTime: DatabaseReference
    private var userNameCompare: String = ""
    lateinit var sharedPref: SharedPreferences
    var currentUser: String = ""
    lateinit var postListenerLoginTime: ValueEventListener
    lateinit var localTime: String
    lateinit var localTimeFinal: Date
    lateinit var name: TextView

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.name -> {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.type = "image/*"
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }

        }
    }

    fun setListener(mListener: UpdateDataListener) {
        this.mListener = mListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setListener(this)
        isHomeLaunched = true
        selectedList = ArrayList()
        finalselectedList = ArrayList()
        finalShareParticipantsList = ArrayList()
        val DATE_TIME_FORMATTER = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

        postReferenceHome = FirebaseDatabase.getInstance().reference
        sharedPref = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        var user = sharedPref.getString(USER_NAME, "").toString().replace(".", ",")
        var dummyUser = sharedPref.getString(USER_NAME, "").toString().replace("@", ".")
        firstName = sharedPref.getString(Constants.FIRST_NAME, "").toString()
        lastName = sharedPref.getString(Constants.LAST_NAME, "").toString()
        userName = sharedPref.getString(USER_NAME, "")
        contactNumberUser = sharedPref.getString(Constants.MOBILE_NUMBER, "")
        contactNumberUser = contactNumberUser.replace("+91", "")
        contactNumberUser = contactNumberUser.replace("-", "")
        contactNumberUser = contactNumberUser.replace(" ", "")
        if (contactNumberUser.startsWith("0")) contactNumberUser = contactNumberUser.substring(1)
        contactNumberUser.trim()
        currentUser = userName.replace(".", ",")
        if (dummyUser != "" && !dummyUser.contains("@")) {
            var subscribed = dummyUser.replace("@", ".")
            // subscribed = subscribed.replace("[0-9]","")
            FirebaseMessaging.getInstance().subscribeToTopic("/topics/" + subscribed)
        }
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) { instanceIdResult ->
            val token = instanceIdResult.token
            Log.e("Token", token)
            if (user != "") {
                postReferenceHome.child("users").child(user).child("fcmToken").setValue(token)
            }
        }
        this.window.setFlags(
            -
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_home)

        userNameCompare = sharedPref.getString(USER_NAME, "").toString().replace(".", ",")



        if (userNameCompare != "") {
            postReferenceHome.child("users").child(userNameCompare).child("login").setValue(true)
            postReferenceHome.child("users").child(userNameCompare).child(Constants.device)
                .setValue(Constants.android)

        }
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        requestBluetoothPermission()

        postReferenceLoginTime =
            postReferenceHome.child("users").child(userNameCompare).child(Constants.loginTime)

        postListenerLoginTime = object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.value != null) {
                    Log.e("loginTime", dataSnapshot.value.toString())
                    val sessionTimeChange = DATE_TIME_FORMATTER.parse(dataSnapshot.value.toString())
                    val compare = sessionTimeChange.compareTo(localTimeFinal)
                    if (compare < 0) {
                        // sessionTimeFinal is earlier
                    } else if (compare > 0) {
                        // localTimeFinal is earlier
                        //  displaySelectedScreen(R.id.nav_signout)
                    } else {
                        // they are equal
                    }

                } else {

                }
            }
        }
        //postReferenceLoginTime.addValueEventListener(postListenerLoginTime)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        toggle.drawerArrowDrawable.color = Color.BLACK
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        val headerview = navView.getHeaderView(0)



        name = headerview.findViewById(R.id.nameHeader) as TextView
        name.setText("" + firstName + " " + lastName)

        navView.setNavigationItemSelectedListener(this)
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.content_frame, HomeFragment())
        ft.commit()

        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading..")
        progressDialog.setCancelable(false)
        progressDialog.show()

        Handler().postDelayed({
            progressDialog.dismiss()
        }, 2000)

        if (intent.hasExtra(Constants.EventID) && intent.hasExtra(Constants.eventType)) {
            if (intent.getStringExtra(Constants.eventType) == "player") {
                var intentvar = Intent(this, PlayerActivityMain::class.java)
                intentvar.putExtra(Constants.eventType, intent.getStringExtra(Constants.eventType))
                intentvar.putExtra(Constants.EventID, intent.getStringExtra(Constants.EventID))
                startActivity(intentvar)
            } else if (intent.getStringExtra(Constants.eventType) == "event") {
                displaySelectedScreen(R.id.nav_schedule)
                addRemainder(intent.getStringExtra(Constants.eventDate))
            } else if (intent.getStringExtra(Constants.eventType) == "eventRemainder") {
                displaySelectedScreen(R.id.nav_schedule)
            }


        }
    }

    // }

    override fun onCreateView(
        parent: View?, name: String, context: Context, attrs: AttributeSet
    ): View? {
        return super.onCreateView(parent, name, context, attrs)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            //super.onBackPressed()

            if (isHomeVisible) {
                finishAffinity()
            } else {
                displaySelectedScreen(R.id.nav_home)
            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        displaySelectedScreen(item.getItemId());
        return true
    }

    private fun displaySelectedScreen(itemId: Int) {

        //creating fragment object
        var fragment: Fragment? = null

        //initializing the fragment object which is selected
        when (itemId) {
            R.id.nav_home -> fragment = HomeFragment()
            R.id.nav_profile -> fragment = ProfileFragment()
            R.id.nav_schedule -> fragment = ScheduleFragment()
            R.id.nav_friends -> fragment = FriendsFragment()
            R.id.nav_signout -> {
                try {
                    var name = sharedPref.getString(USER_NAME, "").toString().replace("@", ".")
                    sharedPref.edit().clear().apply()
                    if (userNameCompare != "") postReferenceHome.child("users").child(
                        userNameCompare
                    ).child(Constants.device).setValue("")
                    FirebaseMessaging.getInstance()
                        .unsubscribeFromTopic("/topics/" + name.toString().replace("[0-9]", ""))
                    if (userNameCompare != "") {
                        postReferenceHome.child("users").child(userNameCompare).child("login")
                            .setValue(false)
                    }
                    postUser = null
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        startActivity(
                            Intent(this, SplashActivity::class.java),
                            ActivityOptions.makeSceneTransitionAnimation(this).toBundle()
                        )
                    } else {
                        startActivity(Intent(this, SplashActivity::class.java))
                    }
                    return
                } catch (e: Exception) {
                    Log.e("ERROR", e.message + "::" + e.cause)
                }
            }
        }

        //replacing the fragment
        if (fragment != null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.content_frame, fragment)
            ft.commit()
        }

        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
    }

    public fun getList(): ArrayList<PersonsModel> {
        var personsListDummy = ArrayList<PersonsModel>()
        var personsFirebaseDummy = ArrayList<Post>()
        try {
            if (post!! != null) {
                if (post!!.containsKey("users")) {
                    var personDetails = post!!.get("users") as HashMap<*, *>
                    var personDetailsKeys = personDetails.keys
                    for (person in personDetailsKeys) {
                        var details = personDetails[person] as HashMap<*, *>
                        personsFirebaseDummy.add(
                            Post(
                                details["email"].toString(), details["firstName"].toString(),
                                details["lastName"].toString(), details["mobileNumber"].toString(),
                                details["password"].toString()
                            )
                        )


                    }
                } else {
                    var personDetails = post!! as HashMap<*, *>
                    var personDetailsKeys = personDetails.keys
                    for (person in personDetailsKeys) {
                        var details = personDetails[person] as HashMap<*, *>
                        personsFirebaseDummy.add(
                            Post(
                                details["email"].toString(), details["firstName"].toString(),
                                details["lastName"].toString(), details["mobileNumber"].toString(),
                                details["password"].toString()
                            )
                        )


                    }
                }

                if (personsFirebaseDummy.size > 0) {
                    for (personItem in personsFirebaseDummy) {

                        if (personItem != null && personItem.email != null && sharedPref.getString(
                                USER_NAME,
                                ""
                            ).toString() != personItem.email
                        ) {
                            personsListDummy.add(
                                PersonsModel(
                                    personItem.firstName,
                                    false,
                                    personItem.email
                                )
                            )
                        }

                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return personsListDummy
    }

    fun setSelectedUser(list: PersonsModel, status: Boolean) {
        try {
            if (status) {
                if (selectedList.size > 0) {
                    for (item in selectedList) {
                        if (item.id != list.id)
                            selectedList.add(list)
                    }
                } else {
                    selectedList.add(list)
                }
            } else {
                selectedList.remove(list)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    fun getSelectedUser(): ArrayList<PersonsModel> {
        try {
            return selectedList
        } catch (e: Exception) {
            e.printStackTrace()
            return selectedList
        }

    }

    fun clearSelectedList() {
        selectedList.clear()
    }

    public fun notificationDetails(id: String) {
        val topic = "/topics/" + id //topic has to match what the receiver subscribed to
        val notification = JSONObject()
        val notifcationBody = JSONObject()

        try {
            notifcationBody.put("title", "Notification")
            notifcationBody.put(
                "message",
                "Your friend wants to watch movie with U!!!"
            )   //Enter your notification message
            notification.put("to", topic)
            notification.put("data", notifcationBody)
            Log.e("TAG", "try")
        } catch (e: JSONException) {
            Log.e("TAG", "onCreate: " + e.message)
        }

        sendNotification(notification)
    }

    public fun sendNotification(notification: JSONObject) {
        Log.e("TAG", "sendNotification")
        val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
            Response.Listener<JSONObject> { response ->
                Log.i("TAG", "onResponse: $response")
                //msg.setText("")
            },
            Response.ErrorListener {
                Toast.makeText(this, "Request error", Toast.LENGTH_LONG).show()
                Log.i("TAG", "onErrorResponse: Didn't work")
            }) {

            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] = serverKey
                params["Content-Type"] = contentType
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }

    val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(this)
    }

    public fun getListMobile(): ArrayList<String> {
        var personsListDummy = ArrayList<String>()
        personsFirebaseDummyArrayList = ArrayList()
        personsFirebaseDummyHashMap = HashMap()


        try {
            if (post!! != null) {
                if (post!!.containsKey("users")) {
                    var personDetails = post!!.get("users") as HashMap<*, *>
                    var personDetailsKeys = personDetails.keys
                    for (person in personDetailsKeys) {
                        var details = personDetails[person] as HashMap<*, *>
                        personsFirebaseDummyArrayList.add(
                            Post(
                                details["email"].toString().replace(".", ","),
                                details["firstName"].toString(),
                                details["lastName"].toString(),
                                details["mobileNumber"].toString(),
                                details["password"].toString()
                            )
                        )
                        personsFirebaseDummyHashMap.put(
                            details["mobileNumber"].toString(),
                            Post(
                                details["email"].toString().replace(".", ","),
                                details["firstName"].toString(),
                                details["lastName"].toString(),
                                details["mobileNumber"].toString(),
                                details["password"].toString()
                            )
                        )


                    }
                } else {
                    var personDetails = post!! as HashMap<*, *>
                    var personDetailsKeys = personDetails.keys
                    for (person in personDetailsKeys) {
                        var details = personDetails[person] as HashMap<*, *>
                        personsFirebaseDummyArrayList.add(
                            Post(
                                details["email"].toString().replace(".", ","),
                                details["firstName"].toString(),
                                details["lastName"].toString(),
                                details["mobileNumber"].toString(),
                                details["password"].toString()
                            )
                        )
                        personsFirebaseDummyHashMap.put(
                            details["mobileNumber"].toString(),
                            Post(
                                details["email"].toString().replace(".", ","),
                                details["firstName"].toString(),
                                details["lastName"].toString(),
                                details["mobileNumber"].toString(),
                                details["password"].toString()
                            )
                        )


                    }
                }

                if (personsFirebaseDummyArrayList.size > 0) {
                    for (personItem in personsFirebaseDummyArrayList) {

                        if (personItem != null && personItem.email != null && userName != personItem.email) {
                            personsListDummy.add(
                                personItem.mobileNumber
                            )
                        }

                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return personsListDummy
    }

    fun addRemainder(eventDate: String) {
        var utcFormat = SimpleDateFormat("dd-mm-YYYY HH:mm:ss", Locale.ENGLISH)
        utcFormat.timeZone = TimeZone.getTimeZone("IST")
        var date = utcFormat.parse(eventDate.replace("T", " "))

        Log.e("Not", "start" + "::" + date.time + "::" + System.currentTimeMillis())
        var intent = Intent()
        intent.setClass(applicationContext, AlarmReceiver::class.java)
        intent.action = "com.example.myapplication.Service.AlarmReceiver"
        var pendingIntent1 = PendingIntent.getBroadcast(
            applicationContext,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        var alarmManager1 =
            applicationContext!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        if (date.time > 15 * 60 * 1000) {
            alarmManager1.setExact(AlarmManager.RTC_WAKEUP, date.time, pendingIntent1)
        } else {
            // alarmManager1.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+60000, pendingIntent1)
        }
        var setTopic = java.util.HashSet<String>()
        var setFCM = java.util.HashSet<String>()

        if (postUser != null /*&& post!!.containsKey("users")*/) {
            var dummy = postUser/*!!.get("users")*/ as HashMap<*, *>
            var currentUserDetails = dummy[currentUser] as HashMap<*, *>

            var deviceDetails = currentUserDetails[Constants.device] as String
            if (deviceDetails != "" && deviceDetails == Constants.android) {
                setTopic.add(currentUser.replace(",", ".").replace("@", "."))
            } else {
                var userid = currentUser.replace(".", ",")
                //var userdetails = dataUsers!!.get(userid) as HashMap<*,*>
                // setFCM.add(userdetails["fcmToken"].toString())
            }
        }

        var sharedPref = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        val editor = sharedPref.edit()
        if (setTopic.size > 0) {
            editor.putStringSet("key_topic", setTopic)
        }
        if (setFCM.size > 0) {
            editor.putStringSet("key_fcm", setFCM)
        }
        editor.apply()


    }

    fun requestBluetoothPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.BLUETOOTH
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.BLUETOOTH
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                requestContactsPermission()
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.BLUETOOTH),
                    0
                )

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
            requestContactsPermission()
        }
    }

    fun requestContactsPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.READ_CONTACTS
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                requestAudioSettings()
                requestForContacts()
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_CONTACTS),
                    1
                )

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            requestAudioSettings()
            requestForContacts()
            // Permission has already been granted
        }
    }

    fun requestAudioSettings() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.MODIFY_AUDIO_SETTINGS
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.MODIFY_AUDIO_SETTINGS
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                requestRecordAudioPermission()
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.MODIFY_AUDIO_SETTINGS),
                    2
                )

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.

            }
        } else {
            // Permission has already been granted
            requestRecordAudioPermission()
        }
    }

    fun requestRecordAudioPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.RECORD_AUDIO
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.RECORD_AUDIO),
                    3
                )
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {

            // Permission has already been granted
        }
    }

    fun setName() {
        if (::name.isInitialized && name != null)
            name.setText("" + firstName + " " + lastName)
    }

    fun requestForContacts() {
        /*progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading..")
        progressDialog.setCancelable(false)
        progressDialog.show()*/
        if (finalShareParticipantsList.size > 0) {
            return
        } else {
            if (post != null) {
                //   doAsync {

                if (contactNumberUser != "" && newPersonsListContacts.contains(contactNumberUser)) {
                    newPersonsListContacts.remove(contactNumberUser)
                }
                try {
                    for (person in newPersonsListForShare) {
                        if (newPersonsListContacts.contains(person)) {
                            /*for (share in personsFirebaseDummy) {
                                if (share.mobileNumber == person) {*/
                            var person = personsFirebaseDummyHashMap[person!!] as Post
                            finalShareParticipantsList.add(
                                PersonsModel(
                                    person.firstName,
                                    false,
                                    person.email
                                )
                            )
                            //}
                            //}
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                var hashSet = HashSet<PersonsModel>()
                hashSet.addAll(finalShareParticipantsList)
                finalShareParticipantsList.clear()
                finalShareParticipantsList.addAll(hashSet)
                //mListener.onRefreshData()
                //RefreshData()
                //claea()
                //var fragment = supportFragmentManager.findFragmentById(R.id.friends_) as
            }

            //   }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            0 -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    requestContactsPermission()
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT)
                        .show()
                }
                return
            }
            1 -> {
                if ((grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    requestAudioSettings()
                    requestForContacts()
                } else {
                    requestContactsPermission()
                }
                return
            }
            2 -> {
                if ((grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    requestRecordAudioPermission()
                    requestForContacts()
                } else {
                    requestAudioSettings()
                }
                return
            }
        }
    }
}
