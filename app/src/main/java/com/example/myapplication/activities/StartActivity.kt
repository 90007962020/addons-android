package com.example.myapplication

import android.Manifest
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.ProgressDialog
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.provider.ContactsContract
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.myapplication.fragments.isLoading
import com.example.myapplication.service.*
import com.google.firebase.database.*
import com.tbruyelle.rxpermissions2.RxPermissions
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

var post: HashMap<*, *>? = null
var postUser: HashMap<*, *>? = null
lateinit var content: ContentResolver
lateinit var postReferenceStart: DatabaseReference
lateinit var postReferenceAll: DatabaseReference
lateinit var postListener: ValueEventListener
lateinit var postListenerAll: ValueEventListener
lateinit var newPersonsListContacts: ArrayList<String>
lateinit var newPersonsListForShare: ArrayList<String>
var personsFirebaseDummyHashMap = HashMap<String, Post>()
var contactListMap: ArrayList<Boolean>? = null
var appContactList: ArrayList<ContactsModel>? = null
var inviteContactList: ArrayList<ContactsModel>? = null
var completeContactList: ArrayList<ContactsModel>? = null
var isLoaded: Boolean = false
var contactNumberUser = ""
var isContactsCalled: Boolean = false

class StartActivity : AppCompatActivity(), DataListener {

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            10 -> {
                // If request is cancelled, the result arrays are empty.
                //Log.e("REquestcode","$requestCode")
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    if (start && userName == "" || userName == null) {
                        /**/
                        Log.e("Splash", "Splash")
                        startSplashActivity()
                    }
                    newPersonsListContacts = getContactsNew()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestContactsPermission()
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    override fun onDataAvailable() {
    }

    private var start: Boolean = false
    private val PREF_NAME = "PREF_NAME"
    private val USER_NAME = "USER_NAME"
    private val PASSWORD = "PASSWORD"
    lateinit var sharedPref: SharedPreferences
    private var userName: String = ""

    private var PRIVATE_MODE = 0
    private var isPlayer: Boolean = false
    private var isEvent: Boolean = false
    private var isEventRemainder: Boolean = false

    lateinit var mListener: DataListener
    lateinit var progressDialog: ProgressDialog

    fun setListener(mListener: DataListener) {
        this.mListener = mListener
    }

    public fun getContactsNew(): ArrayList<String> {
        val arrContacts = ArrayList<String>()
        Log.e("Contacts2", "called ")
        isContactsCalled = true
        contactsList = ArrayList()
        val uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val selection = ContactsContract.Contacts.HAS_PHONE_NUMBER
        var cursor = contentResolver.query(
            uri,
            arrayOf<String>(
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone._ID,
                ContactsContract.Contacts._ID
            ),
            selection,
            null,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
        )
        cursor.moveToFirst()
        while (cursor.isAfterLast === false) {
            var contactNumber =
                cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
            val contactName =
                cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            val phoneContactID =
                cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID))
            val contactID = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts._ID))
            Log.d(
                "con ",
                "name " + contactName + " " + " PhoeContactID " + phoneContactID + " ContactID " + contactID
            )
            cursor.moveToNext()
            contactNumber = contactNumber.replace("+91", "")
            contactNumber = contactNumber.replace("-", "")
            contactNumber = contactNumber.replace(" ", "")
            if (contactNumber.startsWith("0")) contactNumber = contactNumber.substring(1)
            contactNumber.trim()
            if (contactNumberUser != contactNumber) {
                arrContacts.add(contactNumber)
            } else {
                Log.e("AA", "AA" + contactNumberUser)
            }
            //contactsList.add(ContactsModel(contactName,contactNumber))
            contactsHashMap.put(contactNumber, ContactsModel(contactName, contactNumber))
        }
        cursor.close()
        cursor = null
        contactsList.addAll(ArrayList<ContactsModel>(contactsHashMap.values))
        Collections.sort(contactsList, object : Comparator<ContactsModel> {
            override fun compare(lhs: ContactsModel, rhs: ContactsModel): Int {
                //here getTitle() method return app name...
                return lhs.name!!.compareTo(rhs.name!!)
            }
        })
        if (post != null) {
            createFriendsList()
        }
        var hashSet = HashSet<String>()
        hashSet.addAll(arrContacts)
        arrContacts.clear()
        arrContacts.addAll(hashSet)
        return arrContacts
    }

    fun getContacts(): ArrayList<String> {
        val list = ArrayList<String>()
        contactsList = ArrayList()
        var value: Int = 0
        val contentResolver = contentResolver
        val cursor =
            contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
        if (cursor!!.count > 0) {
            while (cursor.moveToNext() /*&& value < 15*/) {
                val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    val cursorInfo = contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf<String>(id),
                        null
                    )
                    /*val inputStream = ContactsContract.Contacts.openContactPhotoInputStream(
                        ctx.getContentResolver(),
                        ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long(id))
                    )*/

                    /* val person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long(id))
                     val pURI = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY)

                     var photo: Bitmap? = null
                     if (inputStream != null) {
                         photo = BitmapFactory.decodeStream(inputStream)
                     }*/
                    while (cursorInfo!!.moveToNext()) {

                        // info.id = id
                        var name =
                            cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                        var number =
                            cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                        /*info.photo = photo
                        info.photoURI = pURI*/
                        number = number.replace("+91", "")
                        number = number.replace("-", "")
                        number = number.replace(" ", "")
                        if (number.startsWith("0")) number = number.substring(1)
                        number.trim()
                        list.add(number)
                        contactsList.add(ContactsModel(name, number))
                        contactsHashMap.put(number, ContactsModel(name, number))
                    }

                    cursorInfo.close()
                    value++
                }
            }
            cursor.close()
        }
        var hashSet = HashSet<String>()
        hashSet.addAll(list)
        list.clear()
        list.addAll(hashSet)

        contactsList = ArrayList<ContactsModel>(contactsHashMap.values)

        Collections.sort(contactsList, object : Comparator<ContactsModel> {
            override fun compare(lhs: ContactsModel, rhs: ContactsModel): Int {
                //here getTitle() method return app name...
                return lhs.name!!.compareTo(rhs.name!!)
            }
        })
        if (post != null) {
            createFriendsList()
        }
        return list
    }

    fun createFriendsList() {
        if (post!!.containsKey("users")) {
            contactListMap = ArrayList()
            appContactList = ArrayList()
            inviteContactList = ArrayList()
            val hashmap = post!!["users"] as HashMap<*, *>
            val keys = hashmap.keys
            var mobileNumber = ArrayList<String>()
            for (key in keys) {
                val userMobile = hashmap[key] as HashMap<*, *>
                if (userMobile.containsKey(Constants.mobileNumber))
                    mobileNumber.add(userMobile.get("mobileNumber")!!.toString())
            }

            for (contact in contactsList) {
                lateinit var user: HashMap<*, *>
                contact.number =
                    contact.number!!.replace("+91", "")

                var size = contactListMap!!.size
                var dummysize = size
                if (mobileNumber.contains(contact.number.toString())) {
                    contactListMap!!.add(true)
                    appContactList!!.add(contact)
                } else {
                    contactListMap!!.add(false)
                    inviteContactList!!.add(contact)
                }


            }

            isLoading = true

        }
    }

    fun requestContactsPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.READ_CONTACTS
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_CONTACTS),
                    10
                )
                Log.e("REQUEST", "10")
                //  newPersonsListContacts = getContactsNew()
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
            if (start && userName == "" || userName == null) {
                /**/
                Log.e("Splash2", "Splash")
                startSplashActivity()
            }
            newPersonsListContacts = getContactsNew()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_start)
        val rxPermissions = RxPermissions(this)
        /* rxPermissions.request(Manifest.permission.READ_CONTACTS)
             .subscribe { result ->
                 if(result){
                     newPersonsListContacts = getContactsNew()
                 }
             }*/
        start = true

        newPersonsListContacts = ArrayList()
        newPersonsListForShare = ArrayList()
        contactListMap = ArrayList()
        appContactList = ArrayList()
        inviteContactList = ArrayList()
        completeContactList = ArrayList()
        sharedPref = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        userName = sharedPref.getString(USER_NAME, "")
        requestContactsPermission()
        contactNumberUser = sharedPref.getString(Constants.MOBILE_NUMBER, "")
        contactNumberUser = contactNumberUser.replace("+91", "")
        contactNumberUser = contactNumberUser.replace("-", "")
        contactNumberUser = contactNumberUser.replace(" ", "")
        if (contactNumberUser.startsWith("0")) contactNumberUser = contactNumberUser.substring(1)
        contactNumberUser.trim()
        /*doAsync {*/
        // }
        setListener(this)
        content = contentResolver

        if (intent.hasExtra(Constants.EventID) && intent.hasExtra(Constants.eventType)) {
            if (intent.getStringExtra(Constants.eventType) == "player") {
                isPlayer = true
            } else if (intent.getStringExtra(Constants.eventType) == "event") {
                isEvent = true
            } else if (intent.getStringExtra(Constants.eventType) == "eventRemainder") {
                isEventRemainder = true
            }
        }
        postListenerAll = object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                post = dataSnapshot.value as HashMap<*, *>
                newPersonsListForShare = HomeActivity().getListMobile()
                if (isContactsCalled) {
                    createFriendsList()
                }
                if (contactNumberUser == "" && userName != "" && post!!.containsKey(userName)) {
                    var finalDetails = post!![userName] as HashMap<*, *>
                    var mobile = finalDetails[Constants.mobileNumber] as String
                    mobile = mobile.replace("+91", "")
                    mobile = mobile.replace("-", "")
                    mobile = mobile.replace(" ", "")
                    if (mobile.startsWith("0")) mobile = mobile.substring(1)
                    mobile.trim()
                    sharedPref.edit().putString(Constants.MOBILE_NUMBER, mobile).commit()
                    contactNumberUser = mobile

                }
                if (newPersonsListForShare.size > 0)
                    HomeActivity().requestForContacts()

                mListener.onDataAvailable()
                Log.e("Listener3", "Listener3")
                postReferenceAll.removeEventListener(postListenerAll)

            }
        }
        postReferenceAll = FirebaseDatabase.getInstance().reference
        postReferenceAll.addValueEventListener(postListenerAll)







        postReferenceStart = FirebaseDatabase.getInstance().reference.child("users")

        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

//Register the receiver

        var calendar = Calendar.getInstance()
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MINUTE, 1)
        calendar.set(Calendar.HOUR_OF_DAY, 12)
        /* calendar.set(Calendar.AM_PM, Calendar.PM)*/
        calendar.add(Calendar.DAY_OF_MONTH, 0)
        /*calendar.add(Calendar.MONTH, 1)
        calendar.add(Calendar.YEAR, 2020)*/

        Log.e(
            "Not",
            "start" + calendar.timeInMillis + "::" + calendar.time + "::" + System.currentTimeMillis()
        )


        postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                if (dataSnapshot.value != null)
                    postUser = dataSnapshot.value as HashMap<*, *>
                var values = postUser!!.get(userName.replace(".", ",").toString()) as HashMap<*, *>
                firstName = values.get(Constants.firstName).toString()
                lastName = values.get(Constants.lastName).toString()
                val editor = sharedPref.edit()
                editor.putString(Constants.FIRST_NAME, firstName)
                editor.putString(Constants.LAST_NAME, lastName)
                editor.apply()
                HomeActivity().setName()
                //PlayerActivityMain().details()

                Log.e("load", "loadPost:" + postUser.toString())
                postReferenceStart.removeEventListener(postListener)

            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("load", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        if (userName != "") {
            var observerName = userName.replace(".", ",")
            postReferenceStart = FirebaseDatabase.getInstance().reference.child("users")
            postReferenceStart.orderByKey().equalTo(observerName)
                .addValueEventListener(postListener)

            Handler().postDelayed({
                if (start) {
                    start = false
                    if (isPlayer) {
                        var intentvar = Intent(this@StartActivity, HomeActivity::class.java)
                        intentvar.putExtra(
                            Constants.eventType,
                            intent.getStringExtra(Constants.eventType)
                        )
                        intentvar.putExtra(
                            Constants.EventID,
                            intent.getStringExtra(Constants.EventID)
                        )
                        startActivityForResult(intentvar, 2)
                    } else if (isEvent) {
                        var intentvar = Intent(this@StartActivity, HomeActivity::class.java)
                        intentvar.putExtra(
                            Constants.eventType,
                            intent.getStringExtra(Constants.eventType)
                        )
                        intentvar.putExtra(
                            Constants.EventID,
                            intent.getStringExtra(Constants.EventID)
                        )
                        intentvar.putExtra(
                            Constants.eventDate,
                            intent.getStringExtra(Constants.eventDate)
                        )
                        startActivityForResult(intentvar, 2)
                    } else if (isEventRemainder) {
                        var intentvar = Intent(this@StartActivity, HomeActivity::class.java)
                        intentvar.putExtra(
                            Constants.eventType,
                            intent.getStringExtra(Constants.eventType)
                        )
                        intentvar.putExtra(
                            Constants.EventID,
                            intent.getStringExtra(Constants.EventID)
                        )
                        intentvar.putExtra(
                            Constants.eventDate,
                            intent.getStringExtra(Constants.eventDate)
                        )
                        startActivityForResult(intentvar, 2)
                    } else {
                        startActivityForResult(
                            Intent(this@StartActivity, HomeActivity::class.java),
                            2
                        )
                    }
                } else if (postUser != null && start) {
                    start = false
                    startActivityForResult(Intent(this@StartActivity, HomeActivity::class.java), 2)
                } else if (start) {
                    startActivityForResult(Intent(this@StartActivity, HomeActivity::class.java), 2)
                }

            }, 1000)


        }


    }

    override fun onPause() {
        super.onPause()
        Log.e("pause post", "pause post")

    }

    override fun onResume() {
        super.onResume()
        Log.e("resume post", "resume post")
        //  postReference.addValueEventListener(postListener)
    }

    fun addReminder(remainder: String) {
        var dateTime = remainder.split("::")
        var date = dateTime[0].split("-")
        var time = dateTime[1].split(":")

        val objAlarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val objCalendar = Calendar.getInstance()
        objCalendar.set(Calendar.YEAR, date[2].toInt())
        objCalendar.set(Calendar.MONTH, date[1].toInt())
        objCalendar.set(Calendar.DAY_OF_MONTH, date[0].toInt())
        objCalendar.set(Calendar.HOUR_OF_DAY, time[0].toInt())
        objCalendar.set(Calendar.MINUTE, time[1].toInt())
        objCalendar.set(Calendar.SECOND, 0)
        objCalendar.set(Calendar.MILLISECOND, 0)
        objCalendar.set(Calendar.AM_PM, Calendar.PM)

        val intent1 = Intent(this, AlarmReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            this,
            0, intent1,
            PendingIntent.FLAG_ONE_SHOT
        )
        val am = this.getSystemService(ALARM_SERVICE) as AlarmManager
        am.set(
            AlarmManager.ELAPSED_REALTIME_WAKEUP,
            System.currentTimeMillis() + 100,
            pendingIntent
        )

    }

    override fun onBackPressed() {
        finishAffinity()
    }

    fun startSplashActivity() {
        Handler().postDelayed({
            start = false
            if (isPlayer) {
                var intentvar = Intent(this@StartActivity, SplashActivity::class.java)
                intentvar.putExtra(Constants.eventType, intent.getStringExtra(Constants.eventType))
                intentvar.putExtra(Constants.EventID, intent.getStringExtra(Constants.EventID))
                startActivityForResult(intentvar, 2)
            } else if (isEvent) {
                var intentvar = Intent(this@StartActivity, SplashActivity::class.java)
                intentvar.putExtra(Constants.eventType, intent.getStringExtra(Constants.eventType))
                intentvar.putExtra(Constants.EventID, intent.getStringExtra(Constants.EventID))
                intentvar.putExtra(Constants.eventDate, intent.getStringExtra(Constants.eventDate))
                startActivityForResult(intentvar, 2)
            } else if (isEventRemainder) {
                var intentvar = Intent(this@StartActivity, SplashActivity::class.java)
                intentvar.putExtra(Constants.eventType, intent.getStringExtra(Constants.eventType))
                intentvar.putExtra(Constants.EventID, intent.getStringExtra(Constants.EventID))
                intentvar.putExtra(Constants.eventDate, intent.getStringExtra(Constants.eventDate))
                startActivityForResult(intentvar, 2)
            } else {
                startActivityForResult(Intent(this@StartActivity, SplashActivity::class.java), 2)
            }
        }, 1000)
    }


}

