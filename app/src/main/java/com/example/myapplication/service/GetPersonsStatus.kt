package com.example.myapplication.service


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.PATCH

interface GetPersonsStatus {

    @PATCH("conferences/id/111/123")
    fun personsStatusCall(
        @Body request: ConferenceCommand
    ): Call<String>
}

data class ConferenceCommand(

    @Expose
    @SerializedName("command")
    var command: String

)





