package com.example.myapplication.service

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.widget.Toast
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import com.android.volley.RequestQueue

import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.myapplication.activities.PlayerActivityMain
import com.example.myapplication.service.Constants
import org.json.JSONObject


private val PREF_NAME = "PREF_NAME"
private var PRIVATE_MODE = 0
private val FCM_API_EVENT = "https://fcm.googleapis.com/fcm/send"
private val serverKey_event = "key=" + "AAAAJ69Wm6w:APA91bG4g5sA29YmgGoqwYNZ5C2BJ1Qa-mbxsar4AENeb6srNTpEBp-t4xJirdw6cmJ3loytyVLEtWMY1lSGHGr3T8RY1d6e0i34FhZHD3NSqG6narKU7gxNnLCWVxO4XfLWXKyjE4ci"
private val contentType_event = "application/json"

class AlarmReceiver : BroadcastReceiver() {
    private lateinit var context: Context
    val requestQueue: RequestQueue by lazy {
        if(context!=null) {
            Volley.newRequestQueue(context)
        }else{
            Volley.newRequestQueue(PlayerActivityMain::class.java.newInstance())
        }
    }

    override fun onReceive(context: Context, cls: Intent) {
       /* val notificationIntent = Intent(context, StartActivity::class.java)*/
        Log.e("Not","Not")
        this.context = context
        var sharedPref = context!!.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        val setTopic = sharedPref.getStringSet("key_topic", null)
        val setFCM = sharedPref.getStringSet("key_fcm", null)
        if(setTopic != null && setTopic.size>0){
            for(id in setTopic){
                var personid = id!!.replace(",", ".").replace("@",".")
                //personid = person.id!!.replace("@", ".")
                val topic =
                    "/topics/" + personid //topic has to match what the receiver subscribed to
                val notification = JSONObject()
                val notifcationBody = JSONObject()
                val notifcationData = JSONObject()
                val notificationTO = JSONObject()
                notification.put("body", "Tap here to watch")
                notification.put(
                    "title",
                     " Time for Movie!!")
                notifcationBody.put(Constants.eventType ,"eventRemainder" )
                notificationTO.put("content_available",true)
                notificationTO.put("priority","high")
                notificationTO.put("data",notifcationBody)
                notificationTO.put("notification",notification)
                notificationTO.put("to", topic)
                sendNotification(notificationTO,context)
            }
        }
        if(setFCM != null && setFCM.size>0){
            for(id in setFCM) {
                val notification = JSONObject()
                val notifcationBody = JSONObject()
                val notifcationData = JSONObject()
                val notificationTO = JSONObject()
                notification.put("body", "Tap here to watch")
                notification.put(
                    "title",
                    " Time for Movie!!"
                )
                notifcationBody.put("eventType", "event")
                notificationTO.put("content_available", true)
                notificationTO.put("priority", "high")
                notificationTO.put("data", notifcationBody)
                notificationTO.put("notification", notification)
                notificationTO.put("to",id)
                sendNotification(notificationTO,context)
            }
        }
       /* notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val stackBuilder = TaskStackBuilder.create(context)
        stackBuilder.addParentStack(StartActivity::class.java)
        stackBuilder.addNextIntent(notificationIntent)
        val pendingIntent = stackBuilder.getPendingIntent(
            0, PendingIntent.FLAG_UPDATE_CURRENT)
        val builder =  NotificationCompat.Builder(context)
        val notification = builder.setContentTitle("Time for Movie")
            .setSmallIcon(R.drawable.newlogo)
            .setContentText("Time for movie").setAutoCancel(true)
            .setContentIntent(pendingIntent).build()
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notification)*/

    }
    public fun sendNotification(notification: JSONObject , context : Context) {
        Log.e("TAG", "sendNotification")
        val jsonObjectRequest = object : JsonObjectRequest(FCM_API_EVENT, notification,
            Response.Listener<JSONObject> { response ->
                Log.i("TAG", "onResponse: $response")
                //msg.setText("")
            },
            Response.ErrorListener {
                Toast.makeText(context, "Request error", Toast.LENGTH_LONG).show()
                Log.i("TAG", "onErrorResponse: Didn't work")
            }) {

            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] = serverKey_event
                params["Content-Type"] = contentType_event
                return params
            }

        }

        requestQueue.add(jsonObjectRequest)
    }

}