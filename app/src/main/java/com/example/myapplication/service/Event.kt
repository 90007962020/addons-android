package com.example.myapplication.service


data class Event(
    var eventName: String? ,
    var movieName: String? ,
    var participants: String? ,
    var participantsNumber: Int? ,
    var date: String? ,
    var time: String?,
    var eventId: String?,
    var eventType: String?
)