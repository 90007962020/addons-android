package com.example.myapplication.service


interface UpdateDataListener {
    fun onRefreshData()
}