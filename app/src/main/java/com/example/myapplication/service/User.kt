package com.example.myapplication.service


data class User(
    var firstName: String? ,
    var lastName: String? ,
    var password: String? ,
    var mobileNumber: String? ,
    var email: String?
)