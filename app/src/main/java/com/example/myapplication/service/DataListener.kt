package com.example.myapplication.service


interface DataListener {
    fun onDataAvailable()
}