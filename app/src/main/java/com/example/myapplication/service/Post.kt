package com.example.myapplication.service

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties


@IgnoreExtraProperties
data class Post(
    var email: String = "",
    var firstName: String = "",
    var lastName: String = "",
    var mobileNumber: String = "",
    var password: String = ""
    //var users: MutableMap<String, String> = HashMap()
) {

    // [START post_to_map]
    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "email" to email,
            "firstName" to firstName,
            "lastName" to lastName,
            "mobileNumber" to mobileNumber,
            "password" to password

        )
    }

}