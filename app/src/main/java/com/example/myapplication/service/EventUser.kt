package com.example.myapplication.service


data class EventUser(
    var date: String? ,
    var time: String?
)