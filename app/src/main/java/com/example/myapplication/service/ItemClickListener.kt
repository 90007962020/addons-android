package com.example.myapplication.service

import com.example.myapplication.ScheduleAdapter


interface adapterItemClickListener {

    fun onAdapterItemClicked(vh: ScheduleAdapter.ViewHolder, item: Any, pos: Int)
}