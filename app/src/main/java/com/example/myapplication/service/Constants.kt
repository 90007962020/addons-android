package com.example.myapplication.service


object Constants{

     val participants = "participants"
     public val creatorId = "creatorId"
     val eventDate = "eventDate"
     val eventId = "eventId"
     val eventTime = "eventTime"
     val eventType = "eventType"
     val eventName = "eventName"
     val isPaused = "isPaused"
     val movieId = "movieId"
     val movieName = "movieName"
     val muteAll = "groupCall"
     val playerPosition = "playerPosition"
     val id = "id"
     val lock = "lock"
     val Ads = "adsOn"
     val personalCall = "inCall"
     val accept = "accept"
     val decline = "decline"
     val name = "name"
     val startTime = "startTime"
     val status = "status"
     val groupCall = "groupCall"
     val EventID = "eventId"
     val positionChange = "positionChange"
     val inviteId = "inviteCall"
     val conferenceId = "conferenceId"
     val device = "device"
     val android = "Android"
     val rewind = "rewind"
     val forward = "forward"
     val loginTime = "loginTime"
     val adTime = "adTime"
     val adNumber = "adNumber"
     val FIRST_NAME = "FIRST_NAME"
     val MOBILE_NUMBER = "MOBILE_NUMBER"
     val LAST_NAME = "LAST_NAME"
     val firstName = "firstName"
     val lastName = "lastName"
     val mobileNumber = "mobileNumber"



}