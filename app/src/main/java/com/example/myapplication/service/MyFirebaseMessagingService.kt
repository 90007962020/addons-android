package com.example.myapplication.service

import android.content.Intent
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import android.R
import android.os.Build
import android.media.RingtoneManager
import android.graphics.BitmapFactory
import android.app.PendingIntent
import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat
import java.util.*
import android.app.NotificationChannel
import android.graphics.Color
import androidx.annotation.RequiresApi
import com.example.myapplication.*

public  var isNewToken : Boolean = false

class MyFirebaseMessagingService : FirebaseMessagingService(){
    private val ADMIN_CHANNEL_ID = "admin_channel"
    private val PREF_NAME = "PREF_NAME"
    private var PRIVATE_MODE = 0

   lateinit var pendingIntent:PendingIntent
    lateinit var notificationManager : NotificationManager
    var notificationID : Int? = 0
    override fun onNewToken(s: String?) {
        super.onNewToken(s)
        Log.e("NEW_TOKEN", s)
        isNewToken = true
    }

     override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)

         val hash = remoteMessage!!.data
         Log.e("Notification","notification")
         if(hash[Constants.eventType] == "player") {
             val intent = Intent(this, StartActivity::class.java)
             intent.putExtra(Constants.EventID, hash[Constants.EventID])
             intent.putExtra(Constants.eventType, hash[Constants.eventType])
              notificationManager =
                 getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
              notificationID = Random().nextInt(3000)


             /*
        Apps targeting SDK 26 or above (Android O) must implement notification channels and add its notifications
        to at least one of them. Therefore, confirm if version is Oreo or higher, then setup notification channel
      */
             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                 setupChannels(notificationManager)
             }

             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
             intent.putExtra("PLAYER", true)
              pendingIntent = PendingIntent.getActivity(
                 this, 0, intent,
                 PendingIntent.FLAG_UPDATE_CURRENT
             )
         }else{
             notificationManager =
                 getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
             notificationID = Random().nextInt(3000)
             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                 setupChannels(notificationManager)
             }
             val intent = Intent(this, StartActivity::class.java)
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
             intent.putExtra(Constants.EventID, hash[Constants.EventID])
             intent.putExtra(Constants.eventType, hash[Constants.eventType])
             intent.putExtra(Constants.eventDate, hash[Constants.eventDate])
             //intent.putExtra("PLAYER", true)
             pendingIntent = PendingIntent.getActivity(
                 this, 0, intent,
                 PendingIntent.FLAG_UPDATE_CURRENT
             )
         }


         val largeIcon = BitmapFactory.decodeResource(
             resources,
             getResources().getIdentifier("newlogo", "drawable", "com.example.myapplication")
         )

         //Bundle[{google.delivered_priority=high, google.sent_time=1578570473922, google.ttl=2419200, google.original_priority=high, gcm.notification.e=1, eventId=appufr@gmail,com09-01-202017:16:59, gcm.notification.title=Annapurna v wants to watch movie with you!!, from=170445413292, eventType=player, google.message_id=0:1578570473928618%5d0d01f35d0d01f3, gcm.notification.body=Tap here to watch, google.c.a.e=1, movieId=1, collapse_key=com.example.myapplication}]
         val notificationSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
         val notificationBuilder = NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
             .setSmallIcon(/*R.mipmap.sym_def_app_icon*/ getResources().getIdentifier("newlogo", "drawable", "com.example.myapplication"))
             /*.setContentText(remoteMessage!!.getData()["title"])
             .setContentTitle(remoteMessage!!.getData()["body"])*/
             .setContentTitle(remoteMessage!!.notification!!.title)
             .setContentText(remoteMessage!!.notification!!.body)
             .setAutoCancel(true)
             .setSound(notificationSoundUri)
             .setContentIntent(pendingIntent)

         //Set notification color to match your app color template
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             notificationBuilder.setColor(resources.getColor(R.color.darker_gray))
         }


         notificationManager.notify(notificationID!!, notificationBuilder.build())


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels(notificationManager: NotificationManager?) {
        val adminChannelName = "New notification"
        val adminChannelDescription = "Device to devie notification"

        val adminChannel: NotificationChannel
        adminChannel = NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_HIGH)
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)
        Log.e("Notification","notification")
        notificationManager?.createNotificationChannel(adminChannel)
    }


}
