package com.example.myapplication.fragments


import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG


import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.*
import com.example.myapplication.service.PersonsModel
import com.example.myapplication.service.UpdateDataListener
import java.util.*
import kotlin.collections.ArrayList
private var adapter: RecyclerView.Adapter<ShareFriendsListAdapter.ViewHolder>? = null
private var adapterEvent: RecyclerView.Adapter<ShareFriendsListAdapterEvent.ViewHolder>? = null
private var layoutManager: RecyclerView.LayoutManager? = null


class ShareFriendsListFragment : DialogFragment() ,View.OnClickListener , UpdateDataListener{
    var notificatinList : ArrayList<PersonsModel>? = null

    var save : TextView? = null
    var cancel : TextView? = null
    override fun onRefreshData() {
        Log.e("Listener2","Listener2")
       /* if(isShareEvent){
            adapterEvent = ShareFriendsListAdapterEvent(this,*//*(context as PlayerActivityMain).getList()*//*
                finalShareParticipantsList,(context as PlayerActivityMain).getTotalParticipants(),(context as PlayerActivityMain).getShareEventPersons())
            recyclerView?.adapter = adapterEvent
        }else if(isNewEvent){
            adapterEvent = ShareFriendsListAdapterEvent(this,*//*(context as PlayerActivityMain).getList()*//*
                finalShareParticipantsList,(context as PlayerActivityMain).getTotalParticipants(),(context as PlayerActivityMain).getNewEventPersons())
            recyclerView?.adapter = adapterEvent
        }else{
            adapter = ShareFriendsListAdapter(this,*//*(context as PlayerActivityMain).getList()*//*
                finalShareParticipantsList)
            recyclerView?.adapter = adapter
        }*/
    }


    override fun onClick(view: View?) {
        when(view!!.id){
            R.id.save-> {
                if(notificatinList != null && notificatinList!!.size > 0)
                createShareEvent(notificatinList!!)
                this.dismiss()
            }
            R.id.cancel->this.dismiss()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.share_friends_list_fragment,
            container, false
        )
        getDialog()!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        return view
    }




    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {

        val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view_friends_list)
        save = view.findViewById(R.id.save)
        cancel = view.findViewById(R.id.cancel)
        layoutManager = LinearLayoutManager(context)
        save!!.setOnClickListener(this)
        cancel!!.setOnClickListener(this)
        recyclerView.layoutManager = layoutManager
        notificatinList = ArrayList()
        if(finalShareParticipantsList.size==0){
            HomeActivity().requestForContacts()
            Toast.makeText(activity,"Syncing Contacts Please wait for some time ..",LENGTH_LONG).show()
            this.dismiss()
        }


        finalShareParticipantsList.sortWith(Comparator { lhs, rhs ->
            lhs.name!!.compareTo(rhs.name!!)
        })
        /*if(isShareEvent){
            adapterEvent = ShareFriendsListAdapterEvent(this,*//*(context as PlayerActivityMain).getList()*//*
                finalShareParticipantsList,(context as PlayerActivityMain).getTotalParticipants(),(context as PlayerActivityMain).getShareEventPersons())
            recyclerView.adapter = adapterEvent
        }else if(isNewEvent){
            adapterEvent = ShareFriendsListAdapterEvent(this,*//*(context as PlayerActivityMain).getList()*//*
                finalShareParticipantsList,(context as PlayerActivityMain).getTotalParticipants(),(context as PlayerActivityMain).getNewEventPersons())
            recyclerView.adapter = adapterEvent
        }else{

            adapter = ShareFriendsListAdapter(this,*//*(context as PlayerActivityMain).getList()*//*
                finalShareParticipantsList)
            recyclerView.adapter = adapter
        }*/


        super.onViewCreated(view, savedInstanceState)


    }
    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            dialog.window!!.setLayout(width, height)
        }
    }
    fun createShareEvent(participantsList : ArrayList<PersonsModel>){
       // (context as PlayerActivityMain).createShareEvent(participantsList)
    }

}

/*fun RefreshData(){
    if(isShareEvent){
        adapterEvent = ShareFriendsListAdapterEvent(
            ShareFriendsListFragment(),*//*(context as PlayerActivityMain).getList()*//*
            finalShareParticipantsList,(ShareFriendsListFragment() as PlayerActivityMain).getTotalParticipants())
        recyclerView!!.adapter = adapterEvent
    }else if(isNewEvent){
        adapterEvent = ShareFriendsListAdapterEvent(ShareFriendsListFragment(),*//*(context as PlayerActivityMain).getList()*//*
            finalShareParticipantsList,(ShareFriendsListFragment() as PlayerActivityMain).getTotalParticipants())
        recyclerView!!.adapter = adapterEvent
    }else{
        adapter = ShareFriendsListAdapter(ShareFriendsListFragment(),*//*(context as PlayerActivityMain).getList()*//*
            finalShareParticipantsList)
        recyclerView!!.adapter = adapter
    }
}*/


