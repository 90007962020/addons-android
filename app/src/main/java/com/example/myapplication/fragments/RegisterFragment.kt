package com.example.myapplication.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton


import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import com.example.myapplication.R


class RegisterFragment : Fragment() ,View.OnClickListener{

    override fun onClick(p0: View?) {
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_registration,
            container, false
        )
        val toolbar: Toolbar = activity!!.findViewById(R.id.toolbar)
        var eventButton = toolbar.findViewById<ImageButton>(R.id.event)
        eventButton.visibility = View.GONE
        return view
    }




    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

}
