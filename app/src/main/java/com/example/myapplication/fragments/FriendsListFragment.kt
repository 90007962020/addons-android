package com.example.myapplication.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.*
import android.content.Intent




class FriendsListFragment : DialogFragment() ,View.OnClickListener{
    private var adapter: RecyclerView.Adapter<FriendsListAdapter.ViewHolder>? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    var save : TextView? = null
    var cancel : TextView? = null
    override fun onClick(view: View?) {
        when(view!!.id){
            R.id.save->{
                //(context as HomeActivity).getSelectedUser()
                sendResult(50,true)
                this.dismiss()
            }
            R.id.cancel->{
                sendResult(50,false)
                this.dismiss()
            }
        }
    }

    private fun sendResult(REQUEST_CODE: Int,status : Boolean) {
        val intent = Intent()
        intent.putExtra("SAVE",status )
        targetFragment!!.onActivityResult(
            targetRequestCode, REQUEST_CODE, intent
        )
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.friends_list_fragment,
            container, false
        )

        return view
    }




    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {

        val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view_friends_list)
        save = view.findViewById(R.id.save)
        cancel = view.findViewById(R.id.cancel)
        save!!.setOnClickListener(this)
        cancel!!.setOnClickListener(this)
        layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        finalShareParticipantsList.sortWith(Comparator { lhs, rhs ->
            lhs.name!!.compareTo(rhs.name!!)
        })
        adapter = FriendsListAdapter(context!!,/*(context as HomeActivity).getList()*/
            finalShareParticipantsList,(context as HomeActivity).getSelectedUser())
        recyclerView.adapter = adapter
        super.onViewCreated(view, savedInstanceState)


    }
    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

}
