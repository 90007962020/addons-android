package com.example.myapplication.fragments

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.InputType.TYPE_NULL
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.example.myapplication.HomeActivity
import com.example.myapplication.R
import com.example.myapplication.service.PersonsModel
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.text.SimpleDateFormat
import java.time.LocalTime
import java.util.*
import kotlin.collections.ArrayList

class EventFragment : DialogFragment(), View.OnClickListener {
    val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context)
    }

    var movieList: EditText? = null
    var eventNameSt: EditText? = null
    var friendsList: EditText? = null
    var createEvent: TextView? = null
    var dateShow: EditText? = null
    var datePicker: DatePickerDialog? = null
    var timePicker: TimePickerDialog? = null
    var timeShow: EditText? = null
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "PREF_NAME"
    private val USER_NAME = "USER_NAME"
    lateinit var sharedPref: SharedPreferences
    private var userName: String = ""
    var str: String? = ""
    lateinit var selectedUserData: ArrayList<PersonsModel>
    private lateinit var database: DatabaseReference
    lateinit var postReferenceShareEvent: DatabaseReference
    lateinit var postReferenceShareEventCreatorID: DatabaseReference
    lateinit var postReferenceShareEventisPaused: DatabaseReference
    lateinit var postReferenceShareEventMuteAll: DatabaseReference
    lateinit var postReferenceShareEventParticipants: DatabaseReference
    lateinit var postReferenceShareEventPlayerPosition: DatabaseReference
    lateinit var postReferenceShareEventPositionChange: DatabaseReference
    var arraylist: ArrayList<String>? = null
    var NEW_EVENT_NAME: String = ""
    private var dummyUser: String = ""
    private val FCM_API_EVENT = "https://fcm.googleapis.com/fcm/send"
    private val serverKey_event =
        "key=" + "AAAAJ69Wm6w:APA91bG4g5sA29YmgGoqwYNZ5C2BJ1Qa-mbxsar4AENeb6srNTpEBp-t4xJirdw6cmJ3loytyVLEtWMY1lSGHGr3T8RY1d6e0i34FhZHD3NSqG6narKU7gxNnLCWVxO4XfLWXKyjE4ci"
    private val contentType_event = "application/json"
    var utcDateString: String = ""
    private var utcDateYear: Int = 0
    private var utcDateMonth: Int = 0
    private var utcDateDay: Int = 0
    lateinit var utcTime: LocalTime
    lateinit var utcDate: GregorianCalendar
    private var eventDate: String = ""
    var createdCalender: Calendar = Calendar.getInstance()
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.create_event -> {
                sharedPref = context!!.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
                userName = sharedPref.getString(USER_NAME, "")
                this.dismiss()
            }
            R.id.movie_list -> {
                val eventFragment = MovieListFragment()
                eventFragment.show(activity!!.supportFragmentManager, "name")
            }
            R.id.friends_list -> {
                val eventFragment = FriendsListFragment()
                eventFragment.setTargetFragment(this, 50)
                eventFragment.show(activity!!.supportFragmentManager, "name")
            }
            R.id.show_date -> datePicker!!.show()
            R.id.show_time -> timePicker!!.show()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(
            R.layout.event_fragment,
            container, false
        )
        sharedPref = activity!!.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        dummyUser = sharedPref.getString(USER_NAME, "").toString().replace(".", ",")
        arraylist = ArrayList()
        return view
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        movieList = view.findViewById(R.id.movie_list)
        eventNameSt = view.findViewById(R.id.eventNameStr)
        friendsList = view.findViewById(R.id.friends_list)
        createEvent = view.findViewById(R.id.create_event)
        dateShow = view.findViewById(R.id.show_date)
        timeShow = view.findViewById(R.id.show_time)
        movieList!!.inputType = TYPE_NULL
        friendsList!!.inputType = TYPE_NULL
        dateShow!!.inputType = TYPE_NULL
        timeShow!!.inputType = TYPE_NULL
        createEvent!!.setOnClickListener(this)
        movieList!!.setOnClickListener(this)
        friendsList!!.setOnClickListener(this)
        dateShow!!.setOnClickListener(this)
        timeShow!!.setOnClickListener(this)
        database = FirebaseDatabase.getInstance().reference.child("users")
        movieList!!.setText("Bahubali")
        val calender = Calendar.getInstance()
        val year = calender.get(Calendar.YEAR)
        val month = calender.get(Calendar.MONTH)
        val day = calender.get(Calendar.DAY_OF_MONTH)
        friendsList!!.isEnabled = true

        datePicker = DatePickerDialog(
            activity!!,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                dateShow!!.setText("$dayOfMonth-$monthOfYear-$year")
                createdCalender.set(Calendar.MONTH, monthOfYear)
                createdCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                createdCalender.set(Calendar.YEAR, year)
                utcDateYear = year
                utcDateMonth = monthOfYear
                utcDateDay = dayOfMonth

            },
            year,
            month,
            day
        )

        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            calender.set(Calendar.HOUR_OF_DAY, hour)
            calender.set(Calendar.MINUTE, minute)
            createdCalender.set(Calendar.HOUR_OF_DAY, hour)
            createdCalender.set(Calendar.MINUTE, minute)
            timeShow!!.setText(SimpleDateFormat("HH:mm").format(calender.time).toString())
            utcDateString += SimpleDateFormat("HH:mm").format(calender.time).toString()
            utcDate = GregorianCalendar(utcDateYear, utcDateMonth, utcDateDay, hour, minute, 0)
            utcDate.timeZone = TimeZone.getTimeZone("UTC")
        }
        timePicker = TimePickerDialog(
            activity!!,
            timeSetListener,
            calender.get(Calendar.HOUR_OF_DAY),
            calender.get(Calendar.MINUTE),
            true
        )


    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 50) {
            var flag = data!!.getBooleanExtra("SAVE", false)
            if (flag) {
                str = ""
                selectedUserData = (context as HomeActivity).getSelectedUser()
                for (person in selectedUserData) {
                    str = str.plus(person.name + " ")

                }
                friendsList!!.setText(str)


            } else {
            }
        }
    }

}
