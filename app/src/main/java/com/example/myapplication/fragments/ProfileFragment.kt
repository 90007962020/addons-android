package com.example.myapplication.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import android.widget.TextView
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import android.content.DialogInterface
import android.widget.ImageButton
import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import com.example.myapplication.*
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class ProfileFragment : Fragment() ,View.OnClickListener{

    var firstName : TextView? = null
    var lastName : TextView? = null
    var mobileNo : TextView? = null
    var emailId : TextView? = null
    var password : TextView? = null
    private lateinit var database: DatabaseReference
    private lateinit var dummyUser:String
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "PREF_NAME"
    private val USER_NAME = "USER_NAME"
    lateinit var userInput:EditText
    lateinit var personDetails : HashMap<*,*>

    override fun onClick(p0: View?) {
        clickAlertDialog(p0!!)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(
            com.example.myapplication.R.layout.fragment_profile,
            container, false
        )
        val toolbar: Toolbar = activity!!.findViewById(com.example.myapplication.R.id.toolbar)
        var eventButton = toolbar.findViewById<ImageButton>(com.example.myapplication.R.id.event)
        eventButton.visibility = View.GONE
        return view
    }




    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //you can set the title for your toolbar here for different fragments different titles
       // activity!!.title = "Menu 1"
        firstName = view.findViewById(com.example.myapplication.R.id.first_name)
        lastName = view.findViewById(com.example.myapplication.R.id.last_name)
        mobileNo = view.findViewById(com.example.myapplication.R.id.mobile_no)
        emailId = view.findViewById(com.example.myapplication.R.id.email_id)
        password = view.findViewById(com.example.myapplication.R.id.password)
        firstName?.setOnClickListener(this)
        lastName?.setOnClickListener(this)
        mobileNo?.setOnClickListener(this)
        emailId?.setOnClickListener(this)
        password?.setOnClickListener(this)
        database = FirebaseDatabase.getInstance().reference
        var sharedPref = activity!!.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        dummyUser = sharedPref.getString(USER_NAME,"").replace(".",",")
        if(post != null) {
            if(post!!.containsKey("users")) {
                var personDetailsUser = post!!.get("users") as HashMap<*, *>
                personDetails = personDetailsUser.get(dummyUser) as HashMap<*, *>


                firstName!!.text = personDetails["firstName"].toString()
                lastName!!.text = personDetails["lastName"].toString()
                mobileNo!!.text = personDetails["mobileNumber"].toString()
                emailId!!.text = personDetails["email"].toString()
                password!!.text = personDetails["password"].toString()
            }else{
                personDetails = post!![dummyUser] as HashMap<*, *>


                firstName!!.text = personDetails["firstName"].toString()
                lastName!!.text = personDetails["lastName"].toString()
                mobileNo!!.text = personDetails["mobileNumber"].toString()
                emailId!!.text = personDetails["email"].toString()
                password!!.text = personDetails["password"].toString()
            }
        }else {
            personDetails = postUser as HashMap<*, *>


            firstName!!.text = personDetails["firstName"].toString()
            lastName!!.text = personDetails["lastName"].toString()
            mobileNo!!.text = personDetails["mobileNumber"].toString()
            emailId!!.text = personDetails["email"].toString()
            password!!.text = personDetails["password"].toString()
        }

    }

    fun clickAlertDialog(view:View){
        val li = LayoutInflater.from(context)
        val promptsView = li.inflate(com.example.myapplication.R.layout.fragment_profile_text, null)
        userInput = promptsView.findViewById(com.example.myapplication.R.id.text_edit_profile) as EditText

        when(view.id){
            com.example.myapplication.R.id.first_name -> {
                userInput.setText(personDetails["firstName"].toString())
            }
            com.example.myapplication.R.id.last_name -> {
                userInput.setText(personDetails["lastName"].toString())
            }
            com.example.myapplication.R.id.mobile_no -> {
                userInput.setText(personDetails["mobileNumber"].toString())
            }
            com.example.myapplication.R.id.email_id -> {
                userInput.setText(personDetails["email"].toString())
            }
            com.example.myapplication.R.id.password ->{
                userInput.setText(personDetails["password"].toString())
            }
        }

        val alertDialogBuilder = AlertDialog.Builder(
            context!!
        )
        alertDialogBuilder.setView(promptsView)


        alertDialogBuilder.setCancelable(false).setPositiveButton("OK") { dialog, id ->
            when(view.id){
                com.example.myapplication.R.id.first_name -> {

                    firstName?.setText(userInput.text)
                    database.child("users").child(dummyUser).child("firstName").setValue(userInput.text.toString())
                }
                com.example.myapplication.R.id.last_name -> {
                    lastName?.setText(userInput.text)
                    database.child("users").child(dummyUser).child("lastName").setValue(userInput.text.toString())
                }
                com.example.myapplication.R.id.mobile_no -> {
                    mobileNo?.setText(userInput.text)
                    database.child("users").child(dummyUser).child("mobileNumber").setValue(userInput.text.toString())
                }
                com.example.myapplication.R.id.email_id -> {
                    emailId?.setText(userInput.text)
                    database.child("users").child(dummyUser).child("email").setValue(userInput.text.toString())
                }
                com.example.myapplication.R.id.password ->{
                    password?.setText(userInput.text)
                    database.child("users").child(dummyUser).child("password").setValue(userInput.text.toString())
                }
            }
        }
            .setNegativeButton("Cancel", object:DialogInterface.OnClickListener {
                override fun onClick(dialog:DialogInterface, id:Int) {
                    dialog.cancel()
                }
            })

        var dialog = alertDialogBuilder.create()
        dialog.show()
    }




}
