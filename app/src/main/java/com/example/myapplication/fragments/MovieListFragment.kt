package com.example.myapplication.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView


import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.*


class MovieListFragment : DialogFragment() ,View.OnClickListener{
    private var adapter: RecyclerView.Adapter<MovieListAdapter.ViewHolder>? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    var save : TextView? = null
    var cancel : TextView? = null
    override fun onClick(view: View?) {
        when(view!!.id){
            R.id.save->this.dismiss()
            R.id.cancel->this.dismiss()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.movie_list_fragment,
            container, false
        )
        val toolbar: Toolbar = activity!!.findViewById(R.id.toolbar)
        var eventButton = toolbar.findViewById<ImageButton>(R.id.event)
        eventButton.visibility = View.GONE
        return view
    }




    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {

        val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view_movies_list)
        save = view.findViewById(R.id.save)
        cancel = view.findViewById(R.id.cancel)
        save!!.setOnClickListener(this)
        cancel!!.setOnClickListener(this)
        layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        adapter = MovieListAdapter()
        recyclerView.adapter = adapter
        super.onViewCreated(view, savedInstanceState)


    }
    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

}
