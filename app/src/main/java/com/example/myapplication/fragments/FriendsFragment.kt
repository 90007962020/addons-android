package com.example.myapplication.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup



import androidx.annotation.Nullable
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import android.provider.ContactsContract
import androidx.fragment.app.DialogFragment
import com.example.myapplication.service.ContactsModel
import android.app.ProgressDialog
import android.widget.ImageButton
import androidx.appcompat.widget.Toolbar
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.myapplication.*
import java.util.HashSet
import io.reactivex.rxjava3.disposables.Disposable
import org.jetbrains.anko.doAsync

private var adapter: RecyclerView.Adapter<FriendsAdapter.ViewHolder>? = null
private var layoutManager: RecyclerView.LayoutManager? = null


lateinit var recyclerView : RecyclerView
lateinit var progressDialog: ProgressDialog
 var isLoading : Boolean = false
var friendsDisposable: Disposable? = null
class FriendsFragment : DialogFragment() ,View.OnClickListener{

    override fun onClick(p0: View?) {
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(
            com.example.myapplication.R.layout.friends_fragment,
            container, false
        )
        val toolbar: Toolbar = activity!!.findViewById(com.example.myapplication.R.id.toolbar)
        var eventButton = toolbar.findViewById<ImageButton>(com.example.myapplication.R.id.event)
        eventButton.visibility = View.GONE

        return view
    }




    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {

         recyclerView = view.findViewById(com.example.myapplication.R.id.recycler_view_friends)
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window.setLayout(width, height)
        }

        layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        progressDialog = ProgressDialog(activity)
        progressDialog.setMessage("Loading..")
        progressDialog.setCancelable(true)
        progressDialog.show()
        /*if(isLoaded){
            isLoading = true
        }else{
            if(isLoading){*/
        displayContacts()
           /* }
        }*/

        var itemRefresh = view.findViewById<SwipeRefreshLayout>(com.example.myapplication.R.id.itemsrefresh)
       /* var friendsObservable: Observable<Boolean> = Observable.interval(1, TimeUnit.SECONDS).map({ isLoading})
        friendsDisposable = friendsObservable.
            subscribeOn(AndroidSchedulers.mainThread())
            .subscribe { progress ->
            if(progress == true){
                if(this.isVisible) {
                    displayContacts()
                    isLoading = false
                }
            }
        }*/
        itemRefresh.setOnRefreshListener {
            completeContactList?.clear()
            completeContactList?.addAll(appContactList!!)
            completeContactList?.addAll(inviteContactList!!)
            adapter = FriendsAdapter(context!!,activity!!.supportFragmentManager,completeContactList!!,contactListMap!!,appContactList!!.size,inviteContactList!!.size)
            recyclerView.adapter = adapter
            itemRefresh.isRefreshing = false
        }


        super.onViewCreated(view, savedInstanceState)


    }

        fun callDisplayContacts() {
            displayContacts()
        }


    fun displayContacts() {
        /* if (post!!.containsKey("users")){
            val hashmap = post!!["users"] as HashMap<*, *>
            val keys = hashmap.keys
            var mobileNumber = ArrayList<String>()
            for (key in keys) {
                val userMobile = hashmap!![key] as HashMap<*, *>
                mobileNumber.add(userMobile.get("mobileNumber")!!.toString())
            }

            for(contact in contactsList) {
               lateinit var user: HashMap<*, *>
               contact.number =
                   contact.number!!.replace("+91", "")


                var size = contactListMap!!.size
                var dummysize = size
                if(mobileNumber.contains(contact.number.toString())){
                    contactListMap!!.add(true)
                    appContactList!!.add(contact)
                }else{
                    contactListMap!!.add(false)
                    inviteContactList!!.add(contact)
                }




            }*/


/*
            val itr = contactsList.listIterator()
            val contactListMapitr = contactListMap?.listIterator()
            val inviteContactListitr = inviteContactList?.listIterator()
            while (itr.hasNext()) {
                val contact = itr.next()

                contact.number =
                    contact.number!!.replace("+91", "")
                *//*if (position == 0) {
                holder.itemEvent.visibility = VISIBLE
                holder.invite.visibility = INVISIBLE
            } else {*//*
                var size = contactListMap!!.size
                var dummysize = size


                // for (key in keys) {
                //    user = hashmap!![key] as HashMap<*, *>
                //  if ((user.get("mobileNumber")!!.toString().equals(contact.number, false))) {
                if(mobileNumber.contains(contact.number.toString())){
                    contactListMapitr!!.add(true)
                    inviteContactListitr!!.add(contact)
                }else{
                    contactListMapitr!!.add(false)
                    inviteContactListitr!!.add(contact)
                }

            }*/


/*
            with(contactsList.iterator()) {
                forEach { contact ->
                    lateinit var user: HashMap<*, *>
                    contact.number =
                        contact.number!!.replace("+91", "")
                    *//*if (position == 0) {
                    holder.itemEvent.visibility = VISIBLE
                    holder.invite.visibility = INVISIBLE
                } else {*//*
                    var size = contactListMap!!.size
                    var dummysize = size


                    // for (key in keys) {
                    //    user = hashmap!![key] as HashMap<*, *>
                    //  if ((user.get("mobileNumber")!!.toString().equals(contact.number, false))) {
                    if(mobileNumber.contains(contact.number.toString())){
                        contactListMap!!.add(true)
                        appContactList!!.add(contact)
                    }else{
                        contactListMap!!.add(false)
                        inviteContactList!!.add(contact)
                    }
            }}*/


        /*for(contact in contactsList) {
                lateinit var user: HashMap<*, *>
                contact.number =
                    contact.number!!.replace("+91", "")
                *//*if (position == 0) {
                holder.itemEvent.visibility = VISIBLE
                holder.invite.visibility = INVISIBLE
            } else {*//*
                var size = contactListMap!!.size
                var dummysize = size


                // for (key in keys) {
                //    user = hashmap!![key] as HashMap<*, *>
                //  if ((user.get("mobileNumber")!!.toString().equals(contact.number, false))) {
                if(mobileNumber.contains(contact.number.toString())){
                    contactListMap!!.add(true)
                    appContactList!!.add(contact)
                }else{
                    contactListMap!!.add(false)
                    inviteContactList!!.add(contact)
                }
                *//*if(dummysize != size) {
                    contactListMap!!.removeAt(size)
                    contactListMap!!.add(true)
                    appContactList!!.add(contact)
                    dummysize++
                }else{
                    contactListMap!!.add(true)
                    appContactList!!.add(contact)
                    dummysize++
                }*//*
                *//*  }else{
                      if (dummysize == size){
                          contactListMap!!.add(false)
                          inviteContactList!!.add(contact)
                          dummysize++
                      }
                  }*//*
                // }
                //}

            }*/
        //  }

        doAsync {
            if (isLoading) {
               // isLoaded = true
                completeContactList?.addAll(appContactList!!)
                completeContactList?.addAll(inviteContactList!!)
                activity!!.runOnUiThread {
                    adapter = FriendsAdapter(
                        context!!,
                        activity!!.supportFragmentManager,
                        completeContactList!!,
                        contactListMap!!,
                        appContactList!!.size,
                        inviteContactList!!.size
                    )
                    recyclerView.adapter = adapter
                    progressDialog.dismiss()
                    //isLoading = false
                }
            } else {
                displayContacts()
            }

        }
    }

    fun getContacts(): ArrayList<ContactsModel> {
        val list = ArrayList<ContactsModel>()
        var value : Int = 0
        val contentResolver = context!!.getContentResolver()
        val cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
        if (cursor!!.getCount() > 0) {
            while (cursor!!.moveToNext()/* && value < 15*/) {
                val id = cursor!!.getString(cursor!!.getColumnIndex(ContactsContract.Contacts._ID))
                if (cursor!!.getInt(cursor!!.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    val cursorInfo = contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", arrayOf<String>(id), null
                    )
                    /*val inputStream = ContactsContract.Contacts.openContactPhotoInputStream(
                        ctx.getContentResolver(),
                        ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long(id))
                    )*/

                   /* val person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long(id))
                    val pURI = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY)

                    var photo: Bitmap? = null
                    if (inputStream != null) {
                        photo = BitmapFactory.decodeStream(inputStream)
                    }*/
                    while (cursorInfo!!.moveToNext()) {

                       // info.id = id
                        var name = cursor!!.getString(cursor!!.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                        var number =
                            cursorInfo!!.getString(cursorInfo!!.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                        /*info.photo = photo
                        info.photoURI = pURI*/
                        number = number.replace("+91","")
                        if(number.startsWith("0")) number = number.substring(1)
                        number.trim()
                        list.add(ContactsModel(name,number))
                    }

                    cursorInfo!!.close()
                    value++
                }
            }
            cursor!!.close()
        }
        var hashSet =  HashSet<ContactsModel>()
        hashSet.addAll(list)
        list.clear()
        list.addAll(hashSet)
        return list
    }

    override fun onDestroy() {
        super.onDestroy()
      //  friendsDisposable!!.dispose()
    }

}

fun refreshFriends(){
    if(progressDialog != null){
        progressDialog.dismiss()
    }
}
