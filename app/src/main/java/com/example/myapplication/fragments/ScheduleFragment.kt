package com.example.myapplication.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.ScheduleAdapter
import com.example.myapplication.activities.PlayerActivityMain
import com.example.myapplication.service.Constants
import com.example.myapplication.service.Event
import com.google.firebase.database.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class ScheduleFragment : Fragment() , View.OnClickListener{


    private var adapter: RecyclerView.Adapter<ScheduleAdapter.ViewHolder>? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private lateinit var postReference: DatabaseReference
    private lateinit var eventListReference: DatabaseReference
   lateinit var postData : HashMap<*,*>
    lateinit var recyclerView: RecyclerView
    lateinit var progressDialog: ProgressDialog
    lateinit var eventList : ArrayList<Event>
    private var users: String = ""
    lateinit var sharedPref: SharedPreferences
    private val USER_NAME = "USER_NAME"
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "PREF_NAME"
    private var isEventExits : Boolean = false
    var currentEventDetails: HashMap<*,*>? = null
    var isDateBefore : Boolean = false
    var isDateEquals : Boolean = false
   lateinit var postListener : ValueEventListener
    override fun onClick(p0: View?) {
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.schedule_profile, container, false)
        postReference = FirebaseDatabase.getInstance().reference
        eventListReference = FirebaseDatabase.getInstance().reference.child("events")
        sharedPref = activity!!.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        var currentUser = sharedPref.getString(USER_NAME, "").toString()
        var userId = currentUser.replace(".",",")
        val toolbar: Toolbar = activity!!.findViewById(R.id.toolbar)
        var eventButton = toolbar.findViewById<ImageButton>(R.id.event)
        eventButton.visibility = VISIBLE
        eventButton.setOnClickListener {
            val eventFragment = EventFragment()
            eventFragment.show(fragmentManager!!, "name")
        }
        Log.e("EVen",eventButton.toString())
        eventList = ArrayList()
         postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                try {

                    var data = dataSnapshot.value as HashMap<*, *>
                    currentEventDetails = data

                    var keys = data.keys
                    for (key in keys) {
                        var details = data[key] as HashMap<*, *>
                        users = ""
                        if (details[Constants.eventType] == "event") {
                            var date = details[Constants.eventDate]
                            if(date != null) {
                                var eventDate =
                                    SimpleDateFormat("dd-MM-yyyy").parse(date.toString())

                                var currentDate = SimpleDateFormat("dd-MM-yyyy").parse(
                                    SimpleDateFormat("dd-MM-yyyy").format(Date(System.currentTimeMillis()))
                                )
                                if (eventDate.before(currentDate)) {
                                    isDateBefore = true

                                }

                                if (eventDate.equals(currentDate)) {
                                    isDateEquals = true
                                }
                                if(isDateEquals || !isDateBefore){

                                var participants = details[Constants.participants] as HashMap<*, *>
                                var participantKeys = participants.keys
                                for (keys in participantKeys) {
                                    var participantDetails = participants[keys] as HashMap<*, *>
                                    if (userId == participantDetails[Constants.id]) {
                                        isEventExits = true
                                    }
                                    users = users + " " + participantDetails[Constants.name]
                                }

                                if (isEventExits) {
                                    var event = Event(
                                        details[Constants.eventName].toString(),
                                        details[Constants.movieName].toString(),
                                        users,
                                        participantKeys.size,
                                        details[Constants.eventDate].toString(),
                                        details[Constants.eventTime].toString(),
                                        details[Constants.eventId].toString(),
                                        details[Constants.eventType].toString()
                                    )
                                    if (!eventList.contains(event) && event.eventName != "null" )
                                        eventList.add(event)
                                }
                            }
                            }
                        }
                    }

                    /*if(postData.containsKey("events")){
                        var data = postData.get("events") as HashMap<*,*>
                        var keys = data.keys
                        for(key in keys){
                            var dummyData = data.get(key) as HashMap<*,*>
                            eventList.add(Event(dummyData["eventName"].toString(),dummyData["movieName"].toString(),dummyData["participants"].toString(),
                                Integer.parseInt(dummyData["participantsNumber"].toString()),dummyData["date"].toString(),dummyData["time"].toString()))
                        }
                    }*/


                    adapter = ScheduleAdapter(context!!,eventList,users)
                    progressDialog.dismiss()
                    recyclerView.adapter = adapter
                    if(eventList.size == 0){
                        Toast.makeText(context,"Nothing to display",LENGTH_LONG).show()
                    }
                }catch (e:Exception){
                    e.printStackTrace()

                   /* adapter = ScheduleAdapter(context!!,eventList,users)
                    progressDialog.dismiss()
                    recyclerView.adapter = adapter*/
                    if(eventList.size == 0){
                       // Toast.makeText(context,"Nothing to display",LENGTH_LONG).show()
                    }
                }


                // ...
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w("load", "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        eventListReference.addValueEventListener(postListener)

        return view
    }


    override fun onPause() {
        super.onPause()
        eventListReference.removeEventListener(postListener)
    }




    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {

       recyclerView = view.findViewById(R.id.recycler_view_schedule)
        layoutManager = LinearLayoutManager(context)
        progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Loading..")
        progressDialog.show()
        recyclerView.layoutManager = layoutManager
        super.onViewCreated(view, savedInstanceState)

    }
    fun startPlayer(){
        var intent = Intent(activity, PlayerActivityMain::class.java)
        intent.putExtra(Constants.EventID,currentEventDetails!![Constants.eventId].toString())
        intent.putExtra(Constants.eventType,currentEventDetails!![Constants.eventType].toString())
        startActivity(intent)

    }

    override fun onResume() {
        super.onResume()
        for(event in eventList){
            if (event.participants == "" || event.eventName == "null"){
                eventList.remove(event)
            }
        }
        adapter = ScheduleAdapter(context!!,eventList,users)
        progressDialog.dismiss()
        recyclerView.adapter = adapter
        eventListReference.addValueEventListener(postListener)
    }


}
