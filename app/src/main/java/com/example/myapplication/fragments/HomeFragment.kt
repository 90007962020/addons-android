package com.example.myapplication.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton


import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.*

var isHomeVisible : Boolean = false

class HomeFragment : Fragment() ,View.OnClickListener{
    private var adapter: RecyclerView.Adapter<HomeAdapter.ViewHolder>? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    override fun onClick(p0: View?) {
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onResume() {
        isHomeVisible = true
        super.onResume()
    }

    override fun onPause() {
        isHomeVisible = false
        super.onPause()
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.home_fragment,
            container, false
        )
        val toolbar: Toolbar = activity!!.findViewById(R.id.toolbar)
        var eventButton = toolbar.findViewById<ImageButton>(R.id.event)
        eventButton.visibility = View.GONE
        return view
    }




    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {

        val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view)
        layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        adapter = HomeAdapter(activity!!)
        recyclerView.adapter = adapter

        super.onViewCreated(view, savedInstanceState)

    }

}
