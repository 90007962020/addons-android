package com.example.myapplication

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.service.Event
import android.content.Intent
import com.example.myapplication.activities.PlayerActivityMain
import com.example.myapplication.service.Constants
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ScheduleAdapter(val context : Context, val eventList : ArrayList<Event>, val users: String) : RecyclerView.Adapter<ScheduleAdapter.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.schedule_profile_item, parent, false)
        return ViewHolder(v)
    }


    override fun getItemCount(): Int {
        return eventList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.context = context
        holder.eventListModel = eventList
        holder.eventName.text = eventList.get(position).eventName
        holder.participants.text = eventList[position].participants
        var utcFormat = SimpleDateFormat("dd-mm-YYYY HH:mm:ss",Locale.ENGLISH)
        //utcFormat.timeZone = TimeZone.getTimeZone("IST")
        var dateList = eventList.get(position).date.toString().split("T")
        var date =  utcFormat.parse(eventList.get(position).date!!.replace("T"," "))
        var utcTime = utcFormat.format(date)
       // holder.dateTime.text = eventList.get(position).date+"  "+eventList.get(position).time
        holder.dateTime.text = dateList[0]+" "+dateList[1]
        holder.itemView.setOnClickListener(View.OnClickListener {
           var eventdetails = eventList.get(position)
            var intent = Intent(context , PlayerActivityMain::class.java)
            intent.putExtra(Constants.EventID,eventdetails.eventId)
            intent.putExtra(Constants.eventType,eventdetails.eventType)
            (context as HomeActivity).startActivity(intent)
        })
        /*holder.eventName.setOnClickListener {

        }*/
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var eventName: TextView
        var context: Context? = null
        var eventListModel: ArrayList<Event>? = ArrayList()
        var dateTime: TextView
        var participants: TextView
        init {

            eventName = itemView.findViewById(R.id.event_name)
            dateTime = itemView.findViewById(R.id.date_time)
            participants = itemView.findViewById(R.id.participants)

        }
    }
}
