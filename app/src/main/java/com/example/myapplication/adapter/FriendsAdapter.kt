package com.example.myapplication

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.service.ContactsModel

class FriendsAdapter(
    val context: Context, val supportFragmentManager: FragmentManager,
    val contactsList: ArrayList<ContactsModel>, val contactsListMap: ArrayList<Boolean>,
    val appListSize: Int, val inviteListSize: Int
) : RecyclerView.Adapter<FriendsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.friends_profile_item, parent, false)


        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        /*if(contactsList.size>4){
            return 4
        }else{*/
        return contactsList.size
        //}

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.context = context
        holder.fragmentManager = supportFragmentManager
        holder.contactsListn = contactsList
        holder.contactsListmap = contactsListMap
        holder.appListSize = appListSize
        holder.inviteListSize = inviteListSize
        holder.name.text = contactsList.get(position).name
        holder.number.text = contactsList.get(position).number
        if (position <= holder.appListSize - 1) {
            holder.itemEvent.visibility = VISIBLE
            holder.invite.visibility = GONE
        } else {
            holder.itemEvent.visibility = GONE
            holder.invite.visibility = VISIBLE
        }
        /*if (holder.contactsListmap!![position] == true){
            holder.itemEvent.visibility = VISIBLE
            holder.invite.visibility = GONE
        }else{
            holder.itemEvent.visibility = GONE
            holder.invite.visibility = VISIBLE
        }*/
        /*if(post!! != null) {
            if (post!!.containsKey("users")){
            val hashmap = post!!["users"] as HashMap<*, *>
            val keys = hashmap.keys
            for (key in keys) {
                val user: HashMap<*, *> = hashmap!![key] as HashMap<*, *>
                contactsList.get(position).number = contactsList.get(position).number!!.replace("+91", "")
                *//*if (position == 0) {
                    holder.itemEvent.visibility = VISIBLE
                    holder.invite.visibility = INVISIBLE
                } else {*//*
                    if ((user.get("mobileNumber")!!.toString().equals(contactsList.get(position).number, false))) {
                        holder.itemEvent.visibility = VISIBLE
                        holder.invite.visibility = GONE
                    } else {
                        holder.itemEvent.visibility = GONE
                        holder.invite.visibility = VISIBLE
                    }
                //}

            }
        }else{
                val hashmap = post!! as HashMap<*, *>
                val keys = post!!.keys
                for (key in keys) {
                    val user: HashMap<*, *> = hashmap!![key] as HashMap<*, *>
                    contactsList.get(position).number = contactsList.get(position).number!!.replace("+91", "")
                    if (position == 0) {
                        holder.itemEvent.visibility = VISIBLE
                        holder.invite.visibility = INVISIBLE
                    } else {
                        if ((user.get("mobileNumber")!!.toString().equals(contactsList.get(position).number, false))) {
                            holder.itemEvent.visibility = VISIBLE
                            holder.invite.visibility = INVISIBLE
                        } else {
                            holder.itemEvent.visibility = INVISIBLE
                            holder.invite.visibility = VISIBLE
                        }
                    }

                }
            }
        }else {
            Toast.makeText(context,"Something went wrong please try again later",Toast.LENGTH_LONG).show()
        }*/


    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        override fun onClick(p0: View?) {
            /* val eventFragment = EventFragment()
             eventFragment.show(fragmentManager!!, "name")*/
        }

        var itemEvent: ImageView
        var name: TextView
        var number: TextView
        var invite: TextView
        var context: Context? = null
        var contactsListn: ArrayList<ContactsModel>? = null
        var contactsListmap: ArrayList<Boolean>? = null
        var fragmentManager: FragmentManager? = null
        var appListSize: Int = 0
        var inviteListSize: Int = 0

        init {
            itemEvent = itemView.findViewById(R.id.event)
            name = itemView.findViewById(R.id.name)
            number = itemView.findViewById(R.id.number)
            invite = itemView.findViewById(R.id.invite)
            itemEvent.setOnClickListener(this)

        }


    }
}
