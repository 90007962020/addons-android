package com.example.myapplication

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import android.content.Intent
import android.graphics.Bitmap
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.core.graphics.drawable.RoundedBitmapDrawable

import android.graphics.BitmapFactory
import com.bumptech.glide.Glide
import com.example.myapplication.activities.PlayerActivityMain
import java.io.ByteArrayOutputStream
import java.lang.Exception


class HomeAdapter(val context: Context) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {


   /* private val images = intArrayOf(R.drawable.test1,
        R.drawable.test2,R.drawable.test3,R.drawable.test4,R.drawable.test5)*/
    private val images = intArrayOf(R.drawable.newimage,
        R.drawable.image2,R.drawable.image3,R.drawable.image4,R.drawable.dummy_img)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_layout, parent, false)
        return ViewHolder(v)
    }


    override fun getItemCount(): Int {
        return 4
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.context = context
        if(position == 0){
            holder.itemImage.visibility = VISIBLE
            holder.textView.visibility = GONE

            try {
                // holder.itemImage.setImageResource(images[position])
                var options = BitmapFactory.Options()
                options.inSampleSize = 2
                var imageBit = BitmapFactory.decodeResource(context.resources, images[position],options)
                holder.itemImage.setImageBitmap(imageBit)
                val RBD = RoundedBitmapDrawableFactory.create(context.resources, imageBit)
                RBD.setCornerRadius(40.0f)
                var byteArrayOutputStream = ByteArrayOutputStream()
                RBD.setAntiAlias(true)
                Glide.with(context).load(RBD).thumbnail( 0.1f ).into(holder.itemImage)
               // holder.itemImage.setImageDrawable(RBD)
            }catch (e: Exception){
                e.printStackTrace()
            }
         //   holder.itemImage.setImageDrawable(RBD)

        }else{
            holder.itemImage.visibility = GONE
            holder.textView.visibility = VISIBLE
            holder.textView.text = "Movie"+position

        }

        holder.frameLayout.setOnClickListener {
            var intent = Intent(context, PlayerActivityMain::class.java)
            intent.putExtra("ITEM_CLICK",position)
            context!!.startActivity(intent)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var itemImage: ImageView
        var frameLayout: RelativeLayout
        var textView: TextView
        var context:Context? = null
        init {
            itemImage = itemView.findViewById(R.id.item_image)
            textView = itemView.findViewById(R.id.homeTextView)
            frameLayout = itemView.findViewById(R.id.frameLayout)

        }


    }
}
