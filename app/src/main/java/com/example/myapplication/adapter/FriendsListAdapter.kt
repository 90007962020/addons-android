package com.example.myapplication

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.service.PersonsModel

class FriendsListAdapter(val context: Context,val personsListShare:ArrayList<PersonsModel>,val selectedList : ArrayList<PersonsModel>) : RecyclerView.Adapter<FriendsListAdapter.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.friends_list_item, parent, false)
        return ViewHolder(v)
    }


    override fun getItemCount(): Int {
        return personsListShare.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.context = context
        holder.personsList = personsListShare
        holder.friendName.setText(personsListShare.get(position).name)
        if(selectedList.contains(personsListShare[position])){
            holder.selectedUser.visibility = VISIBLE
        }else{
            holder.selectedUser.visibility = GONE
        }
       /* for(selectedItem in selectedList){
            if (selectedItem.id == personsListShare.get(position).id){
                holder.selectedUser.visibility = VISIBLE
            }else{
                holder.selectedUser.visibility = GONE
            }
        }*/
        holder.friendsLayout.setOnClickListener( View.OnClickListener() {

            /*if(holder.selectedcontactsList!!.size>0){
                if(holder.selectedcontactsList!!.contains(personsListShare.get(position))){
                    holder.selectedcontactsList!!.remove(personsListShare.get(position))
                    holder.selectedUser.visibility = GONE
                }else{
                    holder.selectedcontactsList!!.add(personsListShare.get(position))
                    holder.selectedUser.visibility = VISIBLE
                }
            }else{
                holder.selectedcontactsList!!.add(personsListShare.get(position))
                holder.selectedUser.visibility = VISIBLE
            }
            (context as HomeActivity).setSelectedUser(holder.selectedcontactsList!!)*/


            if(holder.selectedUser.visibility == GONE){
                holder.selectedUser.visibility = VISIBLE
                (context as HomeActivity).setSelectedUser(personsListShare.get(position),true)
            }else{
                holder.selectedUser.visibility = GONE
                (context as HomeActivity).setSelectedUser(personsListShare.get(position),false)
            }

        })
        /*if(post!! != null) {
            if (post!!.containsKey("users")) {

                val hashmap = post!!["users"] as HashMap<*, *>
                val keys = hashmap.keys
                for (key in keys) {
                    val user: HashMap<*, *> = hashmap!![key] as HashMap<*, *>
                    holder.friendName.text = user["firstName"] as String
                }
            }else {
                val hashmap = post!! as HashMap<*, *>
                val keys = hashmap.keys
                for (key in keys) {
                    val user: HashMap<*, *> = hashmap!![key] as HashMap<*, *>
                    holder.friendName.text = user["firstName"] as String
                }
            }
        }*/
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var friendName: TextView
        var context: Context? = null
        var friendsLayout: RelativeLayout
        var selectedcontactsList: ArrayList<PersonsModel>? = ArrayList()
        var personsList:ArrayList<PersonsModel> = ArrayList()
        var selectedUser : ImageView
        var fragmentManager: FragmentManager? = null
        init {

            friendName = itemView.findViewById(R.id.friend_name)
            selectedUser = itemView.findViewById(R.id.selectedUser)
            friendsLayout = itemView.findViewById(R.id.friends_layout)

        }

    }
}
