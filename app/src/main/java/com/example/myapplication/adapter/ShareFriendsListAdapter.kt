package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.fragments.ShareFriendsListFragment
import com.example.myapplication.service.PersonsModel

class ShareFriendsListAdapter(val context: ShareFriendsListFragment,val personsListShare:ArrayList<PersonsModel>) : RecyclerView.Adapter<ShareFriendsListAdapter.ViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.share_friends_list_item, parent, false)
        return ViewHolder(v)
    }


    override fun getItemCount(): Int {

        return personsListShare.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.context = context
        holder.personsList = personsListShare
        holder.friendName.setText(personsListShare.get(position).name)
        if ((context as ShareFriendsListFragment).notificatinList!!.size >0 && (context as ShareFriendsListFragment).notificatinList!!.contains(personsListShare.get(position))) {
            holder.selectedUser.visibility = VISIBLE
        }else{
            holder.selectedUser.visibility = GONE
        }
        holder.friendsLayout.setOnClickListener( View.OnClickListener() {


                if ((context as ShareFriendsListFragment).notificatinList!!.size > 0 && (context as ShareFriendsListFragment).notificatinList!!.size < 4) {
                    if ((context as ShareFriendsListFragment).notificatinList!!.contains(personsListShare.get(position))) {
                        (context as ShareFriendsListFragment).notificatinList!!.remove(personsListShare.get(position))
                        holder.selectedUser.visibility = GONE
                    } else {
                        (context as ShareFriendsListFragment).notificatinList!!.add(personsListShare.get(position))
                        holder.selectedUser.visibility = VISIBLE
                    }
                } else {
                    if((context as ShareFriendsListFragment).notificatinList!!.size == 0) {
                        (context as ShareFriendsListFragment).notificatinList!!.add(
                            personsListShare.get(
                                position
                            )
                        )
                        holder.selectedUser.visibility = VISIBLE
                    }else{
                        Toast.makeText(context.activity,"Limit Exceeded.", Toast.LENGTH_LONG).show()
                    }
                }

           // (context as PlayerActivityMain).createShareEvent(holder.selectedSharedEventList!!)
            var dummyUser = personsListShare[position].id!!.replace("@",".")
           // if(holder.selectedUser.visibility != VISIBLE)(context as PlayerActivityMain).notificationDetails(dummyUser)


        })
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var friendName: TextView
        var context: ShareFriendsListFragment? = null
        var personsList:ArrayList<PersonsModel> = ArrayList()
        var selectedSharedEventList: ArrayList<PersonsModel>? = ArrayList()
        var friendsLayout: RelativeLayout
        var selectedUser : ImageView
        init {

            friendName = itemView.findViewById(R.id.friend_name)
            friendsLayout = itemView.findViewById(R.id.friends_layout)
            selectedUser = itemView.findViewById(R.id.selectedUser)

        }

    }
}
