package com.example.myapplication.adapter

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myapplication.R
import com.example.myapplication.activities.*
import com.example.myapplication.service.Constants
import com.example.myapplication.service.PersonsModelShare

class PersonListAdapter(val context: Context,val personsList:ArrayList<PersonsModelShare>,var unmutePersons:ArrayList<String>, val userName : String,val name : String) : RecyclerView.Adapter<PersonListAdapter.ViewHolder>(){



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.persons_list_item, parent, false)
        return ViewHolder(v)
    }


    override fun getItemCount(): Int {
        return personsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.context = context
        holder.personsList = personsList
        holder.personName.text = personsList.get(position).name
        if(personsList[position].groupCall){
            holder.personName.setTextColor(/*context!!.resources.getColor(R.color.colorGreen)*/Color.parseColor("#00b300"))
        }else{
            holder.personName.setTextColor(/*context!!.resources.getColor(R.color.colorWhite)*/Color.parseColor("#FFFFFF"))
        }
        var isavailable:Boolean = false
        var currentid = personsList[position].id.toString().replace(".",",")
        for(person in personsList){
            if(outgoingUserId == person.id.toString().replace(".",",") || incomingUserId == person.id.toString().replace(".",",") ){
                if(incomingUserId == person.id.toString().replace(".",",") && !person.available){
                    isavailable = false
                    incomingUserId = ""
                }else if(outgoingUserId == person.id.toString().replace(".",",") && !person.available){
                    isavailable = false
                    outgoingUserId = ""
                }else isavailable = true
            }
        }

        /*if(outgoingUserId == "" && incomingUserId ==""){
            if(personsList.get(position).available ) {
                holder.personStatusCallButton.visibility = GONE
            }else{
                holder.personStatusCallButton.visibility = VISIBLE
            }
        }else {
            if (currentid == outgoingUserId || currentid == incomingUserId) {
                holder.personStatusCallButton.visibility = VISIBLE
             //   holder.personStatus.visibility = VISIBLE
            } else {
                //if(personsList.get(position).available) {
                if(isavailable){
                    if (personsList.get(position).available) {
                        holder.personStatusCallButton.visibility = VISIBLE
                    } else {
                        holder.personStatusCallButton.visibility = GONE
                    }
                }else {
                    if (personsList.get(position).available) {
                        holder.personStatusCallButton.visibility = GONE
                    } else {
                        holder.personStatusCallButton.visibility = VISIBLE
                    }
                }

            }
        }*/

        if(name=="") {
            //  holder.personLayout.setBackgroundResource( R.drawable.participants)

            unmutePersons = ArrayList()
            if (Owner != null && creatorId != null && Owner == creatorId) {
                if (!personsList.get(position).available) {
                    if(personsList[position].inviteId != ""){
                        holder.personStatusEndButton.visibility = VISIBLE
                        holder.personStatusCallButton.visibility = GONE
                        Glide.with(context).load(R.drawable.redcallend).into(holder.personStatusEndButton)
                    }else {
                        holder.personStatusEndButton.visibility = GONE
                        holder.personStatusCallButton.visibility = VISIBLE
                        Glide.with(context).load(R.drawable.greencallanswer)
                            .into(holder.personStatusCallButton)
                    }
                } else if (personsList.get(position).available /*&& !isMute */) {
                    holder.personStatusEndButton.visibility = VISIBLE
                    holder.personStatusCallButton.visibility = GONE
                    Glide.with(context).load(R.drawable.redcallend).into(holder.personStatusEndButton)
                    /*if (callerPerson != null) {
                        callerPerson!!.hangup()
                    }*/
                }
            }else{
                holder.personStatusCallButton.visibility = GONE
                holder.personStatusEndButton.visibility = GONE
                holder.personStatusAnswerButton.visibility = GONE
                holder.personStatusRejectButton.visibility = GONE
                holder.lockStatus.visibility = GONE
                holder.unlockStatus.visibility = GONE
            }

            if (Owner != null && creatorId != null && Owner == creatorId) {
                if(personsList.get(position).lock){
                    holder.lockStatus.visibility = VISIBLE
                    holder.unlockStatus.visibility = GONE
                }else{
                    holder.lockStatus.visibility = GONE
                    holder.unlockStatus.visibility = VISIBLE
                }

            } else {
                holder.lockStatus.visibility = GONE
                holder.unlockStatus.visibility = GONE
            }
            holder.unmutePersons = unmutePersons


        }else if (name != ""){
            var ownerDetails : HashMap<*,*>
            var currentUserDetails : HashMap<*,*>
            if(dataShare == null){
                ownerDetails = dataNewEvent!![creatorId] as HashMap<*,*>
                currentUserDetails = dataNewEvent!![dummyUser] as HashMap<*,*>
            }else{
                ownerDetails = dataShare!![creatorId] as HashMap<*,*>
                currentUserDetails = dataShare!![dummyUser] as HashMap<*,*>
            }


            if (Owner != null && creatorId != null && Owner == creatorId) {

                if(ownerDetails[Constants.personalCall] as Boolean){
                    if(name/*.replace(",",".")*/ == personsList.get(position).id) {
                        holder.personStatusCallButton.visibility = GONE
                        holder.personStatusEndButton.visibility = VISIBLE
                        holder.personStatusAnswerButton.visibility = GONE
                        holder.personStatusRejectButton.visibility = GONE
                        if(personsList.get(position).lock){
                            holder.lockStatus.visibility = VISIBLE
                            holder.unlockStatus.visibility = GONE
                        }else{
                            holder.lockStatus.visibility = GONE
                            holder.unlockStatus.visibility = VISIBLE
                        }
                        //  holder.personLayout.setBackgroundResource( R.drawable.participants)
                    }else{
                        holder.personStatusCallButton.visibility = GONE
                        holder.personStatusEndButton.visibility = GONE
                        holder.personStatusAnswerButton.visibility = GONE
                        holder.personStatusRejectButton.visibility = GONE
                        holder.lockStatus.visibility = GONE
                        holder.unlockStatus.visibility = GONE
                        //   holder.personLayout.setBackgroundResource( R.drawable.participants)
                    }

                }else{
                    holder.personStatusCallButton.visibility = VISIBLE
                    holder.personStatusEndButton.visibility = GONE
                    holder.personStatusAnswerButton.visibility = GONE
                    holder.personStatusRejectButton.visibility = GONE
                    if(personsList.get(position).lock){
                        holder.lockStatus.visibility = VISIBLE
                        holder.unlockStatus.visibility = GONE
                    }else{
                        holder.lockStatus.visibility = GONE
                        holder.unlockStatus.visibility = VISIBLE
                    }
                    //  holder.personLayout.setBackgroundResource(R.drawable.participants)
                }
            }else{
                if(name/*.replace(",",".")*/ == personsList.get(position).id) {
                    if(currentUserDetails[Constants.personalCall] as Boolean){
                        holder.personStatusCallButton.visibility = GONE
                        holder.personStatusEndButton.visibility = VISIBLE
                        holder.personStatusAnswerButton.visibility = GONE
                        holder.personStatusRejectButton.visibility = GONE
                        holder.lockStatus.visibility = GONE
                        holder.unlockStatus.visibility = GONE
                        holder.personLayout.setBackgroundResource(R.drawable.participants_pink)
                    }else{
                        holder.personStatusCallButton.visibility = GONE
                        holder.personStatusEndButton.visibility = GONE
                        holder.personStatusAnswerButton.visibility = VISIBLE
                        holder.personStatusRejectButton.visibility = VISIBLE
                        holder.lockStatus.visibility = GONE
                        holder.unlockStatus.visibility = GONE
                        holder.personLayout.setBackgroundResource( R.drawable.participants_pink)
                    }
                }else{
                    holder.personStatusCallButton.visibility = GONE
                    holder.personStatusEndButton.visibility = GONE
                    holder.personStatusAnswerButton.visibility = GONE
                    holder.personStatusRejectButton.visibility = GONE
                    holder.lockStatus.visibility = GONE
                    holder.unlockStatus.visibility = GONE
                    // holder.personLayout.setBackgroundResource(R.drawable.participants)
                }
            }









/*
            if(name.replace(",",".") == personsList.get(position).id) {
                //Glide.with(context).load(R.drawable.answer).into(holder.personStatusCallButton)
                if (Owner != null && creatorId != null && Owner == creatorId) {
                    if(personsList.get(position).available){
                    holder.personStatusCallButton.visibility = GONE
                    holder.personStatusEndButton.visibility = GONE
                    holder.personStatusAnswerButton.visibility = VISIBLE
                    holder.personStatusRejectButton.visibility = VISIBLE
                    holder.lockStatus.visibility = GONE
                    holder.unlockStatus.visibility = GONE
                    holder.personLayout.setBackgroundColor(context.resources.getColor(R.color.colorPersonsIncomingBackground))
                } else {
                    holder.personStatusCallButton.visibility = GONE
                    holder.personStatusEndButton.visibility = VISIBLE
                    holder.personStatusRejectButton.visibility = GONE
                    holder.personStatusAnswerButton.visibility = GONE
                    if (Owner != null && creatorId != null && Owner == creatorId) {
                        holder.lockStatus.visibility = GONE
                        holder.unlockStatus.visibility = VISIBLE
                    } else {
                        holder.lockStatus.visibility = GONE
                        holder.unlockStatus.visibility = GONE
                    }
                }
            }
            }else{
                holder.personStatusCallButton.visibility = GONE
                holder.personStatusEndButton.visibility = GONE
                holder.personStatusAnswerButton.visibility = GONE
                holder.personStatusRejectButton.visibility = GONE
                holder.lockStatus.visibility = GONE
                holder.unlockStatus.visibility = GONE
            }*/
        }
        /*   holder.personStatus.setOnClickListener( View.OnClickListener() {
               var status = (context as PlayerActivityMain).checkPersonCallStatus(personsList[position].id!!.replace(".",","))

                   var userchanges = personsList[position].id.toString()
                   userchanges = userchanges.replace(".", ",")
                   *//*if (personsList[position].available) {
                    personsList[position].available = false
                } else personsList[position].available = true*//*

                if (personsList[position].available == false) {
                    if(!status) {
                        (context as PlayerActivityMain).disableUsers(position, personsList.size)
                        Glide.with(context).load(R.drawable.unmute).into(holder.personStatus)
                        outgoingUserId = personsList[position].id.toString().replace(".", ",")
                        (context as PlayerActivityMain).callUserChangeStatus(
                            personsList[position].id.toString().replace(
                                ".",
                                ","
                            )
                        )

                        *//* if(position == 0) unmutePersons!!.add("123222")
                else*//*
                        unmutePersons!!.add(personsList[position].id + "222")
                        //   personsList[position].available = true

                        // postReferenceShare.child(SHARE_EVENT_NAME).child("PARTICIPANTS").child(userchanges).child("222").setValue(true)
                        //postReferenceShare.child(SHARE_EVENT_NAME).child("PARTICIPANTS").child(userchanges).child("CALLEDBY").setValue(Owner)
                        Log.e("AAA", "unmute")
                    }else{
                        Toast.makeText(context,personsList[position].name+" is on another call",LENGTH_LONG).show()
                    }

                } else if (personsList[position].available == true) {
                    (context as PlayerActivityMain).enableUsers(position, personsList.size)
                    Glide.with(context).load(R.drawable.mute).into(holder.personStatus)
                    Log.e("BBB", "mute")
                    outgoingUserId = ""
                    (context as PlayerActivityMain).endUser(personsList[position].id.toString().replace(".",","))
                    if (unmutePersons!!.contains(personsList[position].id + "222")) {
                        unmutePersons!!.remove(personsList[position].id + "222")
                        //    personsList[position].available = false

                        //  postReferenceShare.child(SHARE_EVENT_NAME).child("PARTICIPANTS").child(userchanges).child("222").setValue(false)
                    }

                }



                if (context is PlayerActivityMain) {
                   *//* (context as PlayerActivityMain).vibrate()
                    (context as PlayerActivityMain).unmutePersonsStatus(
                        unmutePersons,
                        personsList[position].id.toString()
                    )*//*
                    if(unmutePersons.size>0) {
                        (context as PlayerActivityMain).callSpecificUser(unmutePersons[0],true)
                    }else{
                        (context as PlayerActivityMain).callSpecificUser("",false)
                    }
                }
            *//*}else{

            }*//*
           // notifyItemChanged(position)


      })*/
        holder.personStatusCallButton.setOnClickListener {
            holder.personStatusCallButton.visibility = GONE
            holder.personStatusEndButton.visibility = VISIBLE
            holder.personStatusAnswerButton.visibility = GONE

        }
        holder.personStatusAnswerButton.setOnClickListener {
        }
        holder.personStatusEndButton.setOnClickListener {
        }
        holder.personStatusRejectButton.setOnClickListener {
        }
        holder.lockStatus.setOnClickListener( View.OnClickListener() {
            (context as PlayerActivityMain).vibrate()
            holder.lockStatus.visibility = GONE
            holder.unlockStatus.visibility = VISIBLE
            lockStatus = false

        })
        holder.unlockStatus.setOnClickListener( View.OnClickListener() {
            (context as PlayerActivityMain).vibrate()
            holder.lockStatus.visibility = VISIBLE
            holder.unlockStatus.visibility = GONE
            lockStatus = true

        })
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var personStatusCallButton: ImageButton
        var personStatusAnswerButton: ImageButton
        var personStatusRejectButton: ImageButton
        var personStatusEndButton: ImageButton
        var personLayout: LinearLayout
        var lockStatus: ImageButton
        var unlockStatus: ImageButton
        var personName: TextView
        var context: Context? = null
        var personsList: ArrayList<PersonsModelShare>? = null
        var unmutePersons:ArrayList<String>? = null
        var list: ArrayList<String> = ArrayList()

        init {
            personStatusCallButton = itemView.findViewById(R.id.person_status)
            personStatusAnswerButton = itemView.findViewById(R.id.person_status_answer_call)
            personStatusRejectButton = itemView.findViewById(R.id.person_status_reject_call)
            personLayout = itemView.findViewById(R.id.personLayout)
            personStatusEndButton = itemView.findViewById(R.id.person_status_end_call)
            lockStatus = itemView.findViewById(R.id.lock_status)
            unlockStatus = itemView.findViewById(R.id.unlock_status)
            personName = itemView.findViewById(R.id.person_name)
            unmutePersons = ArrayList()
            personName.setSelected(true)

        }

    }
}
